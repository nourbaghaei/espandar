<div class="box-header">
    <h2>{{__('cms.sign-date')}}</h2>
    <small>{{__('cms.sign-date-services')}}</small>
</div>
<div class="box-divider m-a-0"></div>

@if(is_null($item))
    <div class="form-group row">
        <div class="col-sm-3">
            <span class="text-danger">*</span>
            <label for="sign_time_start" class="form-control-label">{{__('cms.time_start')}} </label>
            <input type="text" name="sign_time_start" value="{{old('sign_time_start')}}" class="form-control" id="sign_time_start"  autocomplete="off">
        </div>
        <div class="col-sm-3">
            <span class="text-danger">*</span>
            <label for="sign_date_start" class="form-control-label" data-provide="datepicker">{{__('cms.date_start')}} </label>
            <input type="text" name="sign_date_start" value="{{old('sign_date_start')}}" class="form-control" id="sign_date_start"  autocomplete="off">
        </div>
        <div class="col-sm-3">
            <span class="text-danger">*</span>
            <label for="sign_time_end" class="form-control-label">{{__('cms.time_end')}} </label>
            <input type="text" name="sign_time_end" value="{{old('sign_time_end')}}" class="form-control" id="sign_time_end" autocomplete="off">
        </div>
        <div class="col-sm-3">
            <span class="text-danger">*</span>
            <label for="sign_date_end" class="form-control-label">{{__('cms.date_end')}} </label>
            <input type="text" name="sign_date_end" value="{{old('sign_date_end')}}" class="form-control" id="sign_date_end"  autocomplete="off">
        </div>

    </div>

@else

    <div class="form-group row">
        <div class="col-sm-3">
            <span class="text-danger">*</span>
            <label for="sign_time_start" class="form-control-label">{{__('cms.time_start')}} </label>
            <input type="text" name="sign_time_start" value="{{Morilog\Jalali\Jalalian::forge($item->sign_start_at)->format('h:m:s')}}" class="form-control" id="sign_time_start"  autocomplete="off">
        </div>
        <div class="col-sm-3">
            <span class="text-danger">*</span>
            <label for="sign_date_start" class="form-control-label" data-provide="datepicker">{{__('cms.date_start')}} </label>
            <input type="text" name="sign_date_start" value="{{Morilog\Jalali\Jalalian::forge($item->sign_start_at)->format('Y/m/d')}}" class="form-control" id="sign_date_start"  autocomplete="off" >
        </div>
        <div class="col-sm-3">
            <span class="text-danger">*</span>
            <label for="sign_time_end" class="form-control-label">{{__('cms.time_end')}} </label>
            <input type="text" name="sign_time_end" value="{{Morilog\Jalali\Jalalian::forge($item->sign_end_at)->format('h:m:s')}}" class="form-control" id="sign_time_end"  autocomplete="off">
        </div>
        <div class="col-sm-3">
            <span class="text-danger">*</span>
            <label for="sign_date_end" class="form-control-label">{{__('cms.date_end')}} </label>
            <input type="text" name="sign_date_end" value="{{Morilog\Jalali\Jalalian::forge($item->sign_end_at)->format('Y/m/d')}}" class="form-control" id="sign_date_end"  autocomplete="off">
        </div>

    </div>




@endif
