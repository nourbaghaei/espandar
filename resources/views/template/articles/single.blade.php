@extends('template.app')

@section('content')
    <!-- Inne Page Banner Area Start Here -->
    <section class="inner-page-banner bg-common inner-page-top-margin overlay-dark-40" data-bg-image="{{asset('template/img/figure/inner-page-banner1.jpg')}}">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumbs-area">

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Inne Page Banner Area End Here -->
    <!-- Blog Area Start Here -->
    <section class="single-blog-wrap padding-top-6 padding-bottom-7 bg--accent">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="single-blog-box">
                        <div class="single-blog-main-thumb">
                            @if(!$item->Hasmedia('images'))
                                <img src="{{asset('img/no-img.gif')}}" alt="{{$item->title}}" title="{{$item->title}}" >
                            @else
                                <img src="{{$item->getFirstMediaUrl('images')}}" alt="{{$item->title}}" title="{{$item->title}}" >
                            @endif
                        </div>
                        <div class="single-blog-content">
                            <ul class="entry-meta">
                                <li><a href="#"><i class="far fa-calendar-alt"></i>اردیبهشت 1398</a></li>
                                <li><a href="#"><i class="fas fa-user"></i>توسط <span> نویسنده</span></a></li>
                                <li><a href="#"><i class="fas fa-comments"></i><span>2</span> نظر</a></li>
                            </ul>
                            <h2 class="item-title">{{$item->title}}</h2>
                            {!! $item->text !!}
                            <ul class="tag-share">
                                <li>
                                    <ul class="inner-tag">
                                        @foreach($item->tags as $tag)
                                        <li>
                                            <a href="#">{{$tag}}</a>
                                        </li>
                                        @endforeach

                                    </ul>
                                </li>
                                <li>
                                    <ul class="inner-share">
                                        <li>
                                            <a href="#">
                                                <i class="fab fa-whatsapp"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fab fa-telegram"></i>
                                            </a>
                                        </li>

                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <ul class="post-view">
                            <li><a href="#"><i class="flaticon-back"></i>مقاله قبلی</a></li>
                            <li><a href="#">مقاله بعدی<i class="flaticon-right-arrow"></i></a></li>
                        </ul>
                        <div class="blog-author">
                            <h3 class="inner-item-heading">درباره نویسنده</h3>
                            <div class="media media-none--xs">
                                @if(!$item->user_info->Hasmedia('images'))
                                    <img src="{{asset('img/no-img.gif')}}" alt="{{$item->user_info->title}}" title="{{$item->user_info->full_name}}" class="rounded-circle media-img-auto" width="80">
                                @else
                                    <img src="{{$item->user_info->getFirstMediaUrl('images')}}" alt="{{$item->user_info->title}}" title="{{$item->user_info->full_name}}" class="rounded-circle media-img-auto" width="80">
                                @endif
                                <div class="media-body">
                                    <h4 class="author-title">{{$item->user_info->full_name}}</h4>
                                    <div class="post-date">اردیبهشت 1398</div>
<p>
   برنامه نویس و طراح سایت
</p>
                                    <ul class="author-social">
                                        <li>
                                            <a href="#"><i class="fab fa-telegra,"></i></a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="fab fa-instagra,"></i></a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="fab fa-whatsapp"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="blog-comment">
                            <h3 class="inner-item-heading"> نظر 2</h3>
                            <ul>
                                <li>
                                    <div class="media media-none--xs">
                                        <img src="{{asset('template/img/blog/reply1.jpg')}}" alt="Comment" class="rounded-circle media-img-auto">
                                        <div class="media-body">
                                            <h4 class="comment-title">جعفر</h4>
                                            <span class="post-date">اردیبهشت 1398</span>
                                            <p>
                                                مرسی بابته مقاله خوبتون
                                            </p>
    <a href="#" class="item-btn">پاسخ</a>
                                        </div>
                                    </div>
                                </li>

                            </ul>
                        </div>
                        <div class="leave-comment-box">
                            <h3 class="inner-item-heading">پیام بگذارید</h3>
                            <form class="leave-form-box">
                                <div class="row gutters-15">
                                    <div class="col-md-4 form-group">
                                        <input type="text" placeholder="نام *" class="form-control" name="Phone"
                                               id="leave-name" data-error="فیلد نام مهم است" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <input type="email" placeholder="ایمیل *" class="form-control" name="email"
                                               id="leave-email" data-error="فیلد ایمیل مهم است" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="col-12 form-group">
                                            <textarea placeholder="پیام شما ..." class="textarea form-control"
                                                      name="message" id="leave-message" rows="7" cols="20" data-error="فیلم پیام مهم است"
                                                      required></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="col-12 form-group margin-b-none">
                                        <button class="item-btn">ارسال پیام</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 sidebar-widget-area sidebar-break-md">

                    <div class="widget widget-box-padding widget-categories">
                        <h3 class="widget-title">دسته بندی ها</h3>
                        <ul class="block-list">

                        </ul>
                    </div>
                    <div class="widget widget-box-padding widget-follow-us">
                        <h3 class="widget-title">ما رو دنبال کنید</h3>
                        <ul class="inline-list">
                            <li class="single-item"><a href="#"><i class="fab fa-telegram"></i></a></li>
                            <li class="single-item"><a href="#"><i class="fab fa-instagram"></i></a></li>
                            <li class="single-item"><a href="#"><i class="fab fa-whatsapp"></i></a></li>

                        </ul>
                    </div>
                    <div class="widget widget-box-padding widget-new-rated">
                        <h3 class="widget-title">پست های اخیر</h3>
                        <ul class="block-list">
                            @foreach($last_articles as $article)
                            <li class="single-item">
                                <div class="item-img">
                                    <a href="{{route('articles.single',['article'=>$article->slug])}}">
                                        @if(!$article->Hasmedia('images'))
                                            <img src="{{asset('img/no-img.gif')}}" alt="{{$article->title}}" title="{{$article->title}}" >
                                        @else
                                            <img src="{{$article->getFirstMediaUrl('images','medium')}}" alt="{{$article->title}}" title="{{$article->title}}" >
                                        @endif
                                    </a>
                                </div>
                                <div class="item-content">
                                    <h4 class="item-title"><a href="#">{{$article->title}}</a></h4>
                                    <div class="item-place"></div>
                                    <ul class="item-rating">
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><i class="fas fa-star"></i></li>
                                    </ul>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="widget widget-box-padding widget-newsletter-subscribe">
                        <h3>مشترک خبرنامه</h3>
                        <p>در خبرنامه ما عضو شوید و از آخرین اخبار خبر داشته باشید</p>
                        <form class="newsletter-subscribe-form">
                            <div class="form-group">
                                <input type="text" placeholder="آدرس ایمیل شما" class="form-control" name="email"
                                       data-error="این فیلد ضروری است" required>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="item-btn">هم اکنون عضو شو</button>
                            </div>
                        </form>
                    </div>
                    <div class="widget widget-box-padding widget-poster">
                        <div class="item-img">
                            <img src="{{asset('template/img/figure/sidebar-figure.jpg')}}" alt="Poster" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Blog Area End Here -->

@endsection
