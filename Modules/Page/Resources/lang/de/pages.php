<?php
return [
    "text-create"=>"you can create your page",
    "text-edit"=>"you can edit your page",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "error"=>"error",
    "index"=>"pages list",
    "singular"=>"page",
    "collect"=>"pages",
    "permission"=>[
        "page-full-access"=>"pages full access",
        "page-list"=>"pages list",
        "page-delete"=>"page delete",
        "page-create"=>"page create",
        "page-edit"=>"edit page",
    ]
];
