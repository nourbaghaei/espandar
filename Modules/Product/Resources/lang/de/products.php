<?php
return [
    "text-create"=>"you can create your product",
    "text-edit"=>"you can edit your product",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"products list",
    "singular"=>"product",
    "collect"=>"products",
    "permission"=>[
        "product-full-access"=>"products full access",
        "product-list"=>"products list",
        "product-delete"=>"product delete",
        "product-create"=>"product create",
        "product-edit"=>"edit product",
    ]
];
