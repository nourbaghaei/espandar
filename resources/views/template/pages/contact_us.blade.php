
@extends('template.app')

@section('content')


    <!--page title start-->

    <section class="page-title o-hidden text-center parallaxie" data-overlay="7" data-bg-img="{{asset('template/images/bg/02.jpg')}}">
        <div class="container">
            <div class="row">

            </div>
            <nav aria-label="breadcrumb" class="page-breadcrumb">

            </nav>
        </div>
    </section>

    <!--page title end-->


    <!--body content start-->

    <div class="page-content">

        <!--contact start-->

        <section class="contact-2">
            <div class="container">
                <div class="row pos-r">
                    <div class="col-lg-8 mr-auto">
                        <div class="contact-main">
                            <h6 class="title mb-4">منتظر انتقادات و پیشنهادات شما هستیم </h6>
                            <form id="contact-form" method="post" action="#">
                                <div class="messages"></div>
                                <div class="form-group">
                                    <input id="form_name" type="text" name="name" class="form-control" placeholder="نام کاربری" required="required" data-error="نام کاربری شما الزامی است.">
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <input id="form_email" type="email" name="email" class="form-control" placeholder="ایمیل" required="required" data-error="ایمیل شما الزامی است.">
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <input id="form_phone" type="tel" name="phone" class="form-control" placeholder="تلفن" required="required" data-error="تلفن شما الزامی است.">
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <textarea id="form_message" name="message" class="form-control" placeholder="پیام شما" rows="4" required="required" data-error="پیام شما الزامی است."></textarea>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <button class="btn btn-border btn-radius"><span>ارسال</span>
                                </button>
                            </form>
                        </div>
                    </div>
                    <div class="form-info theme-bg text-white">
                        <p>
                            از طریق راه های ارتباطی زیر با ما در ارتباط باشید
                        </p>
    <ul class="contact-info list-unstyled mt-4">
                            <li class="mb-4"><i class="flaticon-paper-plane"></i><span>آدرس:</span>
                                <p>{{$setting->address}}</p>
                            </li>
                            <li class="mb-4"><i class="flaticon-phone-call"></i><span>تلفن:</span><a href="tel::{{isset(json_decode($setting->phone,true)[0]) ? json_decode($setting->phone,true)[0] : ""}}">{{isset(json_decode($setting->phone,true)[0]) ? json_decode($setting->phone,true)[0] : ""}}</a>
                            </li>
                            <li><i class="flaticon-message"></i><span>ایمیل</span><a href="mailto::{{$setting->email}}"> {{$setting->email}}</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <!--contact end-->


        <!--map start-->

        <section class="o-hidden p-0">
            <div class="container-fluid p-0">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="map iframe-h">
{!! $setting->google_map !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--map end-->

    </div>

    <!--body content end-->


@endsection

