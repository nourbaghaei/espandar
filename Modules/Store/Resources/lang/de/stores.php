<?php
return [
    "text-create"=>"you can create your store",
    "text-edit"=>"you can edit your store",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"stores list",
    "error"=>"error",
    "singular"=>"store",
    "collect"=>"stores",
    "permission"=>[
        "store-full-access"=>"store full access",
        "store-list"=>"stores list",
        "store-delete"=>"store delete",
        "store-create"=>"store create",
        "store-edit"=>"edit store",
    ]


];
