<?php
return [
    "text-create"=>"you can create your position",
    "text-edit"=>"you can edit your position",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"positions list",
    "error"=>"error",
    "singular"=>"position",
    "collect"=>"positions",
    "permission"=>[
        "position-full-access"=>"position full access",
        "position-list"=>"positions list",
        "position-delete"=>"position delete",
        "position-create"=>"position create",
        "position-edit"=>"edit position",
    ]


];
