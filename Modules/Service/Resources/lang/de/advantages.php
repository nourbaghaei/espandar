<?php
return [
    "text-create"=>"you can create your advantage",
    "text-edit"=>"you can edit your advantage",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "error"=>"error",
    "index"=>"advantages list",
    "singular"=>"advantage",
    "collect"=>"advantages",
    "permission"=>[
        "advantage-full-access"=>"advantages full access",
        "advantage-list"=>"advantages list",
        "advantage-delete"=>"advantage delete",
        "advantage-create"=>"advantage create",
        "advantage-edit"=>"edit advantage",
    ]
];
