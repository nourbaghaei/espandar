<?php


namespace Modules\Carousel\Entities\Repository;


use Modules\Carousel\Entities\Carousel;

class CarouselRepository implements CarouselRepositoryInterface
{

    public function getAll()
    {
       return Carousel::latest()->get();
    }
}
