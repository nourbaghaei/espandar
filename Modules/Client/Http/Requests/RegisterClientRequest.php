<?php

namespace Modules\Client\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class RegisterClientRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname'=>'required',
            'lastname'=>'required',
            'password'=>'min:8',
            'mobile'=>'required|regex:/(09)[0-9]{9}/|unique:clients,mobile,'.$this->token.',token',
            'username'=>'required|unique:clients,username,'.$this->token.',token',
            'email'=>'required|unique:clients,email,'.$this->token.',token'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
