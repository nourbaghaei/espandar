<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\Option;

class ProductProperty extends Model
{
    protected $table='product_properties';

    protected $fillable = ['option'];

    public function option_item(){
        return $this->belongsTo(Option::class,'option','id');
    }
    public function property_item(){
        return $this->belongsTo(OptionProperty::class,'propertyable_id','id');
    }
}
