@extends('template.app')

@section('content')

    <!-- Listing Area Start Here -->
    <section class="inner-page-top-margin padding-top-6 padding-bottom-7 bg--accent">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="filter-box-layout1">
                        <div class="row">
                            <div class="col-md-6">
                                <label class="d-none d-xl-block">جستجوی نزدیکترین رویداد شما؟</label>
                                <div class="input-group stylish-input-group">
                                    <input type="text" placeholder="کافه، مرکز خرید، زیبایی و آمپر؛ آبگرم ..." class="form-control"
                                           name="website" id="form-website" data-error="Search text required" required>
                                    <span class="input-group-addon">
                                            <button type="submit">
                                                <i class="flaticon-search"></i>
                                            </button>
                                        </span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="d-none d-xl-block">محل</label>
                                <div class="input-group stylish-input-group">
                                    <input type="email" placeholder="اینجا شهر یا مکان را تایپ کنید" class="form-control"
                                           name="email" id="form-email" data-error="نام رده مورد نیاز است"
                                           required>
                                    <span class="input-group-addon">
                                            <button type="submit">
                                                <i class="flaticon-placeholder"></i>
                                            </button>
                                        </span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <i class="far fa-calendar-alt"></i>
                                    <input type="text" class="form-control rt-date" placeholder="تاریخ" name="date"
                                           id="form-date" data-error="زمینه موضوع مورد نیاز است" required />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <i class="far fa-clock"></i>
                                    <input type="text" class="form-control rt-time" placeholder="زمان" name="time"
                                           id="form-time" data-error="زمینه موضوع مورد نیاز است" required />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <select class="select2" name="filter-by">
                                        <option value="0">مرتب سازی بر اساس</option>
                                        <option value="1">جدید ترین</option>
                                        <option value="2">فروش بالا</option>
                                        <option value="3">امتیاز بالا</option>
                                        <option value="4">قیمت بالا</option>
                                        <option value="5">قیمت پایین</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item-sorting-wrap">
                        <div class="row">
                            <div class="col-sm-6 col-12 d-flex align-items-center">
                                <div class="item-sorting-title">

                                </div>
                            </div>
                            <div class="col-sm-6 col-12 d-flex align-items-center justify-content-sm-end">
                                <div class="layout-switcher">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="listing-view" class="listing-box-grid">
                        <div class="row">
                            @foreach($items as $item)
                            <div class="col-md-6">
                                <div class="product-box">
                                    <div class="item-img bg--gradient-50">
                                        <div class="item-status status-open active">فعال</div>
                                        @if(!$item->Hasmedia('images'))
                                            <img src="{{asset('img/no-img.gif')}}" alt="Listing" class="img-fluid grid-view-img">
                                            <img src="{{asset('img/no-img.gif')}}" alt="Listing" class="img-fluid list-view-img">
                                        @else
                                            <img src="{{$item->getFirstMediaUrl('images')}}" alt="Listing" class="img-fluid grid-view-img">
                                            <img src="{{$item->getFirstMediaUrl('images')}}" alt="Listing" class="img-fluid list-view-img">
                                        @endif

                                        <ul class="item-rating">
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><span>8.4<span> / 10</span></span> </li>
                                        </ul>
                                        <div class="item-logo">
                                            @if(!$item->Hasmedia('logo'))
                                                <img src="{{asset('img/no-img.gif')}}" alt="Listing"  width="80">
                                            @else
                                                <img src="{{$item->getFirstMediaUrl('logo')}}" alt="Listing" width="80">
                                            @endif
                                        </div>
                                    </div>
                                    <div class="item-content">
                                        <h3 class="item-title"><a href="{{route('companies.single',['company'=>$item->id])}}">{{$item->company->name}}</a></h3>
                                        <p class="item-paragraph">{{$item->company->about}}
                                        </p>
                                        <ul class="contact-info">
                                            <li><i class="fas fa-map-marker-alt"></i>{{$item->company->address}}</li>
                                            <li><i class="flaticon-phone-call"></i>{{$item->company->phone}}</li>
                                            <li><i class="fas fa-globe"></i>{{$item->company->website}}</li>
                                        </ul>
                                        <ul class="meta-item">
                                            <li class="item-btn"><a href="#" class="btn-fill">جزئیات</a></li>
                                            <li class="ctg-name"><a href="#"><i class="flaticon-chef"></i>{{get_category($item->category)}}</a></li>
                                            <li class="entry-meta">
                                                <ul>
                                                    <li class="tooltip-item ctg-icon" data-tips="Restaurant"><a
                                                            href="#"><i class="flaticon-chef"></i></a></li>
                                                    <li class="tooltip-item" data-tips="مورد علاقه من"><a href="#"><i
                                                                class="fas fa-heart"></i></a></li>
                                                    <li class="tooltip-item" data-tips="گالری"><a href="#"><i
                                                                class="far fa-image"></i></a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>

                    </div>
                </div>
                <div class="col-lg-4 sidebar-widget-area sidebar-break-md ">
                    <div class="widget widget-box-padding widget-poster">
                        <div class="item-img">
                            <img src="{{asset('template/img/figure/sidebar-figure.jpg')}}" alt="Poster" class="img-fluid">
                        </div>
                    </div>
                    <div class="widget widget-box-padding widget-new-rated">
                        <h3 class="widget-title">آگهی های جدید برای فروش</h3>
                        <ul class="block-list">
{{--                            <li class="single-item">--}}
{{--                                <div class="item-img">--}}
{{--                                    <a href="#"><img src="img/blog/post1.jpg" alt="Post"></a>--}}
{{--                                </div>--}}
{{--                                <div class="item-content">--}}
{{--                                    <h4 class="item-title"><a href="#">هتل تهران</a></h4>--}}
{{--                                    <div class="item-place">شهر تهران</div>--}}
{{--                                    <ul class="item-rating">--}}
{{--                                        <li><i class="fas fa-star"></i></li>--}}
{{--                                        <li><i class="fas fa-star"></i></li>--}}
{{--                                        <li><i class="fas fa-star"></i></li>--}}
{{--                                        <li><i class="fas fa-star"></i></li>--}}
{{--                                        <li><i class="fas fa-star"></i></li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                            </li>--}}
                        </ul>
                    </div>
                    <div class="widget widget-contact-info">
                        <div class="bg-icon">
                            <i class="flaticon-phone-call"></i>
                        </div>
                        <h4 class="item-title">نیاز به هر گونه اطلاعات دارید؟</h4>
                        <p>سوال خود را از کارشناسان ما بپرسید</p>
                        <div class="contact-number">09195995044</div>
                    </div>
                    <div class="widget widget-box-padding widget-latest-article">
                        <h3 class="widget-title">آخرین مقالات</h3>
                        <ul class="block-list">
                            @foreach($last_articles as $article)
                            <li class="single-item">
                                <h4 class="item-title"><a href="{{route('articles.single',['article'=>$article->slug])}}">{{$article->title}}</a></h4>
                                <div class="post-date">{{Morilog\Jalali\Jalalian::forge($article->create_at)->format('%A, %d %B %y')}}</div>
                            </li>
                            @endforeach

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Listing Area End Here -->


@endsection


