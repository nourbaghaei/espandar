<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user');
            $table->foreign('user')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('category');
            $table->foreign('category')->references('id')->on('categories')->onDelete('cascade');
            $table->string('title')->unique();
            $table->string('slug');
            $table->text('text');
            $table->integer('order')->nullable()->default(1);
            $table->string('country');
            $table->string('refer_link')->nullable();
            $table->string('short_link')->nullable();
            $table->text('address')->nullable();
            $table->text('google_map')->nullable();
            $table->string('longitude')->nullable();
            $table->string('latitude')->nullable();
            $table->string('excerpt');
            $table->timestamp('sign_start_at')->nullable();
            $table->timestamp('sign_end_at')->nullable();
            $table->timestamp('start_at');
            $table->timestamp('end_at');
            $table->tinyInteger('status')->default(1);
            $table->string('token')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
