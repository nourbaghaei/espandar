
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h2>{{__('cms.search')}} </h2>

            </div>
            <div class="box-divider m-a-0"></div>
            <div class="box-body">
                @include('core::layout.alert-danger')
                <form role="form" method="post" action="{{route('search.order')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row">
                        <div class="col-sm-3">
                            <label for="order_id" class="form-control-label">{{__('cms.order_id')}}  </label>
                            <input type="text" value="{{isset($request->order_id) ? $request->order_id :""}}" name="order_id" class="form-control" id="order_id" >
                        </div>

                    </div>

                    <div class="form-group row m-t-md">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary btn-sm text-sm">{{__('cms.search')}} </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

