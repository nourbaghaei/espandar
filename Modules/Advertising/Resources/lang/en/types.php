<?php
return [
    "text-create"=>"you can create your type",
    "text-edit"=>"you can edit your type",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"types list",
    "error"=>"error",
    "singular"=>"type",
    "collect"=>"types",
    "permission"=>[
        "type-full-access"=>"type full access",
        "type-list"=>"types list",
        "type-delete"=>"type delete",
        "type-create"=>"type create",
        "type-edit"=>"edit type",
    ]


];
