<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::group(["prefix"=>config('cms.prefix-admin'),"middleware"=>["auth:web"]],function() {
    Route::resource('/informations', 'InformationController')->only('create','store','destroy','update','index','edit');

    Route::group(["prefix"=>'search'], function () {
        Route::post('/informations', 'InformationController@search')->name('search.information');
    });

    Route::group(["prefix"=>'information/categories'], function () {
        Route::get('/', 'InformationController@categories')->name('information.categories');
        Route::get('/create', 'InformationController@categoryCreate')->name('information.category.create');
        Route::post('/store', 'InformationController@categoryStore')->name('information.category.store');
        Route::get('/edit/{category}', 'InformationController@categoryEdit')->name('information.category.edit');
        Route::patch('/update/{category}', 'InformationController@categoryUpdate')->name('information.category.update');

    });

    Route::group(["prefix"=>'information/questions'], function () {
        Route::get('/{information}', 'InformationController@question')->name('information.questions');
        Route::get('/create/{information}', 'InformationController@questionCreate')->name('information.question.create');
        Route::post('/store/{information}', 'InformationController@questionStore')->name('information.question.store');
        Route::delete('/destroy/{question}', 'InformationController@questionDestroy')->name('information.question.destroy');
        Route::get('/edit/{information}/{question}', 'InformationController@questionEdit')->name('information.question.edit');
        Route::patch('/update/{question}', 'InformationController@questionUpdate')->name('information.question.update');
    });


});
