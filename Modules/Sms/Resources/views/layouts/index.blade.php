@extends('core::layout.panel')
@section('pageTitle', 'لیست پیام رسان ها')
@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-sm-12">
                <div class="box">
                    <div class="box-header">
                        <div class="pull-left">
                            <h2>پیام رسان ها</h2>
                            <small>پیام رسان ها</small>
                        </div>
                        <a href="{{route('smses.create')}}" class="btn btn-sm text-sm text-sm-center btn-primary pull-right">ایجاد</a>
                    </div>
                    <div class="table-responsive">

                        <table class="table table-striped b-t">
                            <thead>
                            <tr>
                                <th>شرکت</th>
                                <th>توکن</th>
                                <th>وضعیت</th>
                                <th>تاریخ ثبت</th>
                                <th>گزینه‌ها</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($smses as $item)
                                <tr>
                                    @include('sweet::alert')
                                    <td>{{$item->provider}}</td>
                                    <td>{{$item->key}}</td>
                                    <td>{!! \Modules\Sms\library\SmsHelper::smsStatus($item->status) !!}</td>
                                    <td>{{$item->created_at->ago()}}</td>
                                    <td>

                                        <a href="{{route('smses.show',['smse' => $item->token])}}" class="btn btn-primary btn-sm text-sm">مشاهده</a>

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
