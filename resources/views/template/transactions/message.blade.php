<!DOCTYPE html>
<html lang="fa">

<head>
    <meta charset="utf-8" />
    @if(!$setting->Hasmedia('logo'))
        <link rel="shortcut icon" href="{{asset('img/no-img.gif')}}">
        <link rel="apple-touch-icon" href="{{asset('img/no-img.gif')}}">
    @else
        <link rel="shortcut icon" href="{{$setting->getFirstMediaUrl('logo')}}">
        <link rel="apple-touch-icon" href="{{$setting->getFirstMediaUrl('logo')}}">
    @endif
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport' />
    <title>بازگشت از پرداخت</title>
    <!--     Fonts and icons     -->
    <link rel="stylesheet" href="{{asset('template/fonts/font-awesome/css/font-awesome.min.css')}}" />
    <!-- CSS Files -->
    <link href="{{asset('template/css/bootstrap.min.css')}}" rel="stylesheet" />
    <link href="{{asset('template/css/now-ui-kit.css')}}" rel="stylesheet" />
    <link href="{{asset('template/css/plugins/owl.carousel.css')}}" rel="stylesheet" />
    <link href="{{asset('template/css/plugins/owl.theme.default.min.css')}}" rel="stylesheet" />
    <link href="{{asset('template/css/main.css')}}" rel="stylesheet" />
</head>

<body>

<div class="wrapper default shopping-page">
    <!-- header-shopping -->
    <header class="header-shopping default">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center pt-2">
                    <div class="header-shopping-logo default">
                        <a href="#">
                            <img src="{{asset('template/img/logo.png')}}" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-12 text-center">
                    <ul class="checkout-steps">
                        <li>
                            <a href="#" class="active">
                                <span>اطلاعات ارسال</span>
                            </a>
                        </li>
                        <li class="active">
                            <a href="#" class="active">
                                <span>پرداخت</span>
                            </a>
                        </li>
                        <li class="active">
                            <a href="#" class="active">
                                <span>اتمام خرید و ارسال</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </header>
    <!-- header-shopping -->

    <!-- main-shopping -->
    <main class="cart-page default">
        <div class="container">
            <div class="row">
                @if($error==1 || $error==0)

                    <div class="cart-page-content col-12 order-1">
                        <section class="page-content default">
                            <div class="warning-checkout text-center default">
                                <div class="icon-warning">
                                    <i class="fa fa-close"></i>
                                </div>
                                <h1>سفارش <a href="#">{{\Modules\Payment\Entities\Payment::whereToken($payment)->first()->order}}</a>ثبت شد اما پرداخت ناموفق بود.</h1>
                                <p class="text-warning">برای جلوگیری از لغو سیستمی سفارش،تا 24 ساعت آینده پرداخت را انجام دهید.</p>
                                <p>چنانچه طی این فرایند مبلغی از حساب شما کسر شده است،طی 72 ساعت آینده به حساب شما باز خواهد گشت.</p>
                            </div>
                            <div class="order-info default">
                                <h3>کد سفارش: <span>{{\Modules\Payment\Entities\Payment::whereToken($payment)->first()->order}}</span></h3>
                                <p>سفارش شما با موفقیت در سیستم ثبت شد و هم اکنون <span
                                        class="badge badge-warning">در انتظار پرداخت</span> است.جزئیات این سفارش را می توانید
                                    با کلیک بر روی دکمه <a href="#" class="btn-link-border">پیگیری سفارش</a>مشاهده نمایید.</p>
                                <button type="button" class="btn-primary">
                                    پیگیری سفارش
                                </button>
                                <div class="table-responsive default mt-3 mb-3">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th scope="col">نام تحویل گیرنده : {{$client->cart->first_name}} {{$client->cart->last_name}}</th>
                                            <th scope="col">شماره تماس : {{$client->cart->mobile}}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <th scope="row">تعداد مرسوله : 1</th>
                                            <td>مبلغ کل : {{$client->cart->items->sum('price')}}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">وضعیت پرداخت : پرداخت آنلاین(ناموفق)</th>
                                            <td>وضعیت سفارش: در انتظار پرداخت</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">آدرس : {{$client->cart->address}}</th>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </section>
                    </div>


                @elseif($error==2)

                    <div class="cart-page-content col-12 order-1">
                        <section class="page-content default">
                            <div class="success-checkout text-center default">
                                <div class="icon-success">
                                    <i class="fa fa-check"></i>
                                </div>
                                <h1>سفارش <a href="#">{{\Modules\Payment\Entities\Payment::whereToken($payment)->first()->order}}</a>با موفقیت در سیستم ثبت شد.</h1>
                                <p>سفارش نهایتا تا یک روز آماده ارسال خواهد شد.</p>
                            </div>
                            <div class="order-info default">
                                <h3>کد سفارش: <span>{{\Modules\Payment\Entities\Payment::whereToken($payment)->first()->order}}</span></h3>
                                <p>سفارش شما با موفقیت در سیستم ثبت شد و هم اکنون <span
                                        class="badge badge-success">تکمیل شده</span> است.جزئیات این سفارش را می توانید
                                    با کلیک بر روی دکمه <a href="#" class="btn-link-border">پیگیری سفارش</a>مشاهده نمایید.</p>
                                <button type="button" class="btn-primary">
                                    پیگیری سفارش
                                </button>
                                <div class="table-responsive default mt-3 mb-3">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th scope="col">نام تحویل گیرنده : {{$client->cart->first_name}} {{$client->cart->last_name}}</th>
                                            <th scope="col">شماره تماس : {{$client->cart->mobile}}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <th scope="row">تعداد مرسوله : 1</th>
                                            <td>مبلغ کل : {{$client->cart->items->sum('price')}}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">وضعیت پرداخت : پرداخت آنلاین(موفق)</th>
                                            <td>وضعیت سفارش: در انتظار پرداخت</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">آدرس : {{$client->cart->address}}</th>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </section>
                    </div>

                @endif

            </div>
        </div>
    </main>
    <!-- main-shopping -->

    <!-- footer -->
    <footer class="main-footer default">
        <div class="back-to-top">
            <a href="#"><span class="icon"><i class="now-ui-icons arrows-1_minimal-up"></i></span> <span>بازگشت به
                        بالا</span></a>
        </div>
        <div class="container">
            <div class="footer-services">
                <div class="row">
                    <div class="service-item col">
                        <a href="#" target="_blank">
                            <img src="{{asset('template/img/svg/delivery.svg')}}">
                        </a>
                        <p>تحویل اکسپرس</p>
                    </div>
                    <div class="service-item col">
                        <a href="#" target="_blank">
                            <img src="{{asset('template/img/svg/contact-us.svg')}}">
                        </a>
                        <p>پشتیبانی 24 ساعته</p>
                    </div>
                    <div class="service-item col">
                        <a href="#" target="_blank">
                            <img src="{{asset('template/img/svg/payment-terms.svg')}}">
                        </a>
                        <p>پرداخت درمحل</p>
                    </div>
                    <div class="service-item col">
                        <a href="#" target="_blank">
                            <img src="{{asset('template/img/svg/return-policy.svg')}}">
                        </a>
                        <p>۷ روز ضمانت بازگشت</p>
                    </div>
                    <div class="service-item col">
                        <a href="#" target="_blank">
                            <img src="{{asset('template/img/svg/origin-guarantee.svg')}}">
                        </a>
                        <p>ضمانت اصل بودن کالا</p>
                    </div>
                </div>
            </div>
            <div class="footer-widgets">
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-3">
                        <div class="widget-menu widget card">
                            <header class="card-header">
                                <h3 class="card-title">راهنمای خرید از آسون کالا</h3>
                            </header>
                            <ul class="footer-menu">

                            </ul>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3">
                        <div class="widget-menu widget card">
                            <header class="card-header">
                                <h3 class="card-title">خدمات مشتریان</h3>
                            </header>
                            <ul class="footer-menu">

                            </ul>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3">
                        <div class="widget-menu widget card">
                            <header class="card-header">
                                <h3 class="card-title">با آسون کالا</h3>
                            </header>
                            <ul class="footer-menu">

                            </ul>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3">
                        <div class="newsletter">
                            <p>از تخفیف‌ها و جدیدترین‌های فروشگاه باخبر شوید:</p>
                            <form action="">
                                <input type="email" class="form-control"
                                       placeholder="آدرس ایمیل خود را وارد کنید...">
                                <input type="submit" class="btn btn-primary" value="ارسال">
                            </form>
                        </div>
                        <div class="socials">
                            <p>ما را در شبکه های اجتماعی دنبال کنید.</p>
                            <div class="footer-social">
                                <a href="#" target="_blank"><i class="fa fa-instagram"></i>اینستاگرام آسون کالا</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="info">
                <div class="row">
                    <div class="col-12 col-lg-4">
                        <span>هفت روز هفته ، 24 ساعت شبانه‌روز پاسخگوی شما هستیم.</span>
                    </div>
                    <div class="col-12 col-lg-2">شماره تماس: 021-123456789</div>
                    <div class="col-12 col-lg-2">آدرس ایمیل:<a href="#">info@digikala.com</a></div>
                    <div class="col-12 col-lg-4 text-center">
                        <a href="#" target="_blank"><img src="assets/img/bazzar.png" width="159" height="48"
                                                         alt=""></a>
                        <a href="#" target="_blank"><img src="assets/img/sibapp.png" width="159" height="48"
                                                         alt=""></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="description">
            <div class="container">
                <div class="row">
                    <div class="site-description col-12 col-lg-7">
                        <h1 class="site-title">فروشگاه اینترنتی آسون کالا، بررسی، انتخاب و خرید آنلاین</h1>
                        <p>
                            آسون کالا به عنوان یکی از قدیمی‌ترین فروشگاه های اینترنتی با بیش از یک دهه تجربه، با
                            پایبندی به سه اصل کلیدی، پرداخت در
                            محل، 7 روز ضمانت بازگشت کالا و تضمین اصل‌بودن کالا، موفق شده تا همگام با فروشگاه‌های
                            معتبر جهان، به بزرگ‌ترین فروشگاه
                            اینترنتی ایران تبدیل شود. به محض ورود به آسون کالا با یک سایت پر از کالا رو به رو
                            می‌شوید! هر آنچه که نیاز دارید و به
                            ذهن شما خطور می‌کند در اینجا پیدا خواهید کرد.
                        </p>
                    </div>
                    <div class="symbol col-12 col-lg-5">
                        <a href="#" target="_blank"><img src="{{asset('template/img/symbol-01.png')}}" alt=""></a>
                        <a href="#" target="_blank"><img src="{{asset('template/img/symbol-02.png')}}" alt=""></a>
                    </div>
                    <div class="col-12">
                        <div class="row">
                            <ul class="footer-partners default">
                                <li class="col-md-3 col-sm-6">
                                    <a href=""><img src="{{asset('template/img/footer/1.svg')}}" alt=""></a>
                                </li>
                                <li class="col-md-3 col-sm-6">
                                    <a href=""><img src="{{asset('template/img/footer/2.svg')}}" alt=""></a>
                                </li>
                                <li class="col-md-3 col-sm-6">
                                    <a href=""><img src="{{asset('template/img/footer/3.svg')}}" alt=""></a>
                                </li>
                                <li class="col-md-3 col-sm-6">
                                    <a href=""><img src="{{asset('template/img/footer/4.svg')}}" alt=""></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright">
            <div class="container">
                <p>
                    استفاده از مطالب فروشگاه اینترنتی آسون کالا فقط برای مقاصد غیرتجاری و با ذکر منبع بلامانع است.
                    کلیه حقوق این سایت متعلق
                    به فروشگاه آنلاین آسون کالا می‌باشد.
                </p>
            </div>
        </div>
    </footer>
    <!-- footer -->
</div>

</body>
<!--   Core JS Files   -->
<script src="{{asset('template/js/core/jquery.3.2.1.min.js')}}" type="text/javascript"></script>
<script src="{{asset('template//js/core/popper.min.js')}}" type="text/javascript"></script>
<script src="{{asset('template/js/core/bootstrap.min.js')}}" type="text/javascript"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="{{asset('template/js/plugins/bootstrap-switch.js')}}"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="{{asset('template/js/plugins/nouislider.min.js')}}" type="text/javascript"></script>
<!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
<script src="{{asset('template/js/plugins/bootstrap-datepicker.js')}}" type="text/javascript"></script>
<!-- Share Library etc -->
<script src="{{asset('template/js/plugins/jquery.sharrre.js')}}" type="text/javascript"></script>
<!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
<script src="{{asset('template/js/now-ui-kit.js')}}" type="text/javascript"></script>
<!--  CountDown -->
<script src="{{asset('template/js/plugins/countdown.min.js')}}" type="text/javascript"></script>
<!--  Plugin for Sliders -->
<script src="{{asset('template/js/plugins/owl.carousel.min.js')}}" type="text/javascript"></script>
<!--  Jquery easing -->
<script src="{{asset('template/js/plugins/jquery.easing.1.3.min.js')}}" type="text/javascript"></script>
<!-- Main Js -->
<script src="{{asset('template/js/main.js')}}" type="text/javascript"></script>

</html>
