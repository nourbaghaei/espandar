
<ul id="main-menu" class="nav navbar-nav mr-auto">
    @foreach($top_menus as $menu)
    <li class="nav-item"> <a class="nav-link" href="{{$menu->href}}"><span class="menu-label">{{$menu->symbol}} </span></a>

    </li>
    @endforeach

</ul>
