<?php

namespace Modules\Menu\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;
use Modules\Article\Entities\Article;
use Modules\Core\Entities\Category;
use Modules\Event\Entities\Event;
use Modules\Information\Entities\Information;
use Modules\Menu\Entities\ListMenu;
use Modules\Menu\Entities\Menu;
use Modules\Menu\Entities\Repository\MenuRepositoryInterface;
use Modules\Menu\Http\Requests\MenuRequest;
use Modules\Menu\Transformers\MenuCollection;
use Modules\Page\Entities\Page;
use Modules\Portfolio\Entities\Portfolio;
use Modules\Product\Entities\Product;

class MenuController extends Controller
{

    protected $entity;

    private $repository;

    public function __construct(MenuRepositoryInterface $repository)
    {
        $this->entity=new Menu();

        $this->repository=$repository;

        $this->middleware('permission:menu-list')->only('index');
        $this->middleware('permission:menu-create')->only(['create','store']);
        $this->middleware('permission:menu-edit' )->only(['edit','update']);
        $this->middleware('permission:menu-delete')->only(['destroy']);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            $items=$this->repository->getAll();

            $result = new MenuCollection($items);

            $data= collect($result->response()->getData())->toArray();

            return view('core::response.index',compact('data'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }

    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {

        try {
            $listmenus=ListMenu::latest()->get();
            $parent_menus=$this->entity->latest()->whereParent(0)->get();
            $pages=Page::latest()->get();
            $portfolios=Portfolio::latest()->get();
            $products=Product::latest()->get();
            $menus=$this->entity->latest()->get();
            $events=Event::latest()->get();
            $articles=Article::latest()->get();
            $informations=Information::latest()->get();
            $categories_product=Category::latest()->whereModel(Product::class)->get();
            return view('menu::menus.create',compact('listmenus','pages','menus','parent_menus','portfolios','products','articles','informations','events','categories_product'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        try {
            if(
                !isset($request->title) &&
                !isset($request->order) &&
                !isset($request->symbol) &&
                !isset($request->href)
            ){

                $items=$this->repository->getAll();

                $result = new MenuCollection($items);

                $data= collect($result->response()->getData())->toArray();

                return view('core::response.index',compact('data'));
            }
             $items=$this->entity
                ->where("title",'LIKE','%'.trim($request->title).'%')
                ->where("order",'LIKE','%'.trim($request->order).'%')
                ->where("symbol",'LIKE','%'.trim($request->symbol).'%')
                ->where("href",'LIKE','%'.trim($request->href).'%')
                ->paginate(config('cms.paginate'));
            $result = new MenuCollection($items);

            $data= collect($result->response()->getData())->toArray();

            return view('core::response.index',compact('data'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param MenuRequest $request
     * @return void
     */
    public function store(MenuRequest $request)
    {

        try {
            DB::beginTransaction();

            $parent=-1;
            if($request->input('parent')!==-1){
                $parent=$this->entity->whereToken($request->input('parent'))->first();
            }

            $href=(is_null($request->manual)) ? (is_null($request->href) ? '#': $request->href) : trim($request->manual);

            $this->entity->user=auth('web')->user()->id;
            $this->entity->title=$request->input('title');
            $this->entity->symbol=$request->input('symbol');
            $this->entity->icon=$request->input('icon');
            $this->entity->parent=($request->input('parent')==-1) ? 0: $parent->id;
            $this->entity->order=orderInfo($request->input('order'));
            $this->entity->column=orderInfo($request->input('column'));
            $this->entity->columns=orderInfo($request->input('columns'));
            $this->entity->href=$href;
            $this->entity->List_menus=ListMenu::whereToken($request->input('list'))->firstOrFail()->id;
            $this->entity->token=tokenGenerate();

            $saved=$this->entity->save();

            if(!$saved){
                DB::rollBack();
                    return redirect()->back()->with('error',__('menu::menus.error'));
            }else{
                DB::commit();
                return redirect(route("menus.index"))->with('message',__('menu::menus.store'));
            }



        }catch (Exception $exception){
            DB::rollBack();
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @param $token
     * @return Response
     */
    public function edit($token)
    {

        try {

            $parent_menus=$this->entity->latest()->whereParent(0)->where('token','!=',$token)->get();
            $listmenus=ListMenu::latest()->get();
            $item=$this->entity->whereToken($token)->first();
            $pages=Page::latest()->get();
            $portfolios=Portfolio::latest()->get();
            $products=Product::latest()->get();
            $events=Event::latest()->get();
            $articles=Article::latest()->get();
            $informations=Information::latest()->get();
            $categories_product=Category::latest()->whereModel(Product::class)->get();
            $menus=$this->entity->latest()->get();
            return view('menu::menus.edit',compact('item','listmenus','pages','menus','parent_menus','portfolios','products','events','articles','informations','categories_product'));
        }catch (\Exception $exception){
            sendMailErrorController($exception);
            return abort('500');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param MenuRequest $request
     * @param int $id
     * @return void
     */
    public function update(MenuRequest $request, $token)
    {


        try {
            DB::beginTransaction();
            $parent=-1;
            if($request->input('parent')!==-1){
                $parent=$this->entity->whereToken($request->input('parent'))->first();
            }

            $href=(is_null($request->manual)) ? (is_null($request->href) ? '#': $request->href) : trim($request->manual);
             $this->entity=$this->entity->whereToken($token)->firstOrFail();
            $updated=$this->entity->update([
                "user"=>auth('web')->user()->id,
                "title"=>$request->input('title'),
                "symbol"=>$request->input('symbol'),
                "icon"=>$request->input('icon'),
                "parent"=>($request->input('parent')==-1) ? 0: $parent->id,
                "order"=>orderInfo($request->input('order')),
                "column"=>orderInfo($request->input('column')),
                "columns"=>orderInfo($request->input('columns')),
                "href"=>$href,
                "list_menus"=>ListMenu::whereToken($request->input('list'))->firstOrFail()->id,
            ]);

            if(!$updated){
                DB::rollBack();
                return redirect()->back()->with('error',__('menu::menus.error'));
            }else{
                DB::commit();
                return redirect(route("menus.index"))->with('message',__('menu::menus.update'));
            }

        }catch (Exception $exception){
            DB::rollBack();
            sendMailErrorController($exception);
            return abort('500');

        }
    }

    /**
     * Remove the specified resource from storage.
     * @param $token
     * @return void
     */
    public function destroy($token)
    {

        try {
            DB::beginTransaction();
            $this->entity=$this->entity->whereToken($token)->firstOrFail();
            $deleted=$this->entity->delete();

            if(!$deleted){
                DB::rollBack();
                return redirect()->back()->with('error',__('menu::menus.error'));
            }else{
                DB::commit();
                return redirect(route("menus.index"))->with('message',__('menu::menus.delete'));
            }
        }catch (\Exception $exception){
            DB::rollBack();
            sendMailErrorController($exception);
            return abort('500');
        }
    }
}
