<?php


namespace Modules\Advertising\Entities\Repository;


use Modules\Advertising\Entities\Skill;

class SkillRepository implements SkillRepositoryInterface
{

    public function getAll()
    {
        return Skill::latest()->get();
    }
}
