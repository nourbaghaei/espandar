<?php

namespace Modules\Client\Http\Requests;

use App\Rules\Nationalcode;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class ClientUpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'=>'nullable|unique:clients,email,'.$this->client.',id',
            'mobile'=>'required|regex:/(09)[0-9]{9}/|max:11|unique:clients,mobile,'.$this->client.',id',
            'username'=>'unique:clients,username,'.$this->client.',id',
            'address'=>'nullable',
            'postal_code'=>'nullable|numeric|max:10',
            'image'=>'mimes:jpeg,png,jpg|max:200',
            'identity_card' => ['nullable',new Nationalcode()]
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
