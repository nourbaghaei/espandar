<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class UserIdentifierCode extends Model
{
    protected $table="user_identifier_codes";

    protected $fillable = ['count','expire'];

}
