<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Core\Entities\Country;
use Modules\Core\Entities\State;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cities')->delete();

        $cities=[

            [
                'name' => 'Tehran',
                'state' => State::where('name','Tehran')->first()->id,
                'country' => Country::where('name','Iran')->first()->id,
                'created_at' =>now(),
            ],
            [
                'name' => 'Shahriar',
                'state' => State::where('name','Tehran')->first()->id,
                'country' => Country::where('name','Iran')->first()->id,
                'created_at' =>now(),
            ],
            [
                'name' => 'Eslamshahr',
                'state' => State::where('name','Tehran')->first()->id,
                'country' => Country::where('name','Iran')->first()->id,
                'created_at' =>now(),
            ]


        ];

        DB::table('cities')->insert($cities);
    }
}
