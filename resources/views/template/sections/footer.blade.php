




<

<!--footer start-->

<footer class="footer dark-bg pt-10 sm-pt-8 pos-r" data-bg-img="images/bg/09.png" style="background-size: contain; background-repeat: no-repeat;">
    <div class="contact-media">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="media-icon style-2 list-inline theme-bg">
                        <li> <i class="flaticon-paper-plane"></i>
                            <span>آدرس:</span>
                            <p class="mb-0">{{$setting->address}}</p>
                        </li>
                        <li> <i class="flaticon-phone-call"></i>
                            <span>تلفن:</span>
                            <a href="tel::{{isset(json_decode($setting->phone,true)[0]) ? json_decode($setting->phone,true)[0] : ""}}">{{isset(json_decode($setting->phone,true)[0]) ? json_decode($setting->phone,true)[0] : ""}}</a>
                        </li>
                        <li> <i class="flaticon-message"></i>
                            <span>ایمیل:</span>
                            <a href="mailto::{{$setting->email}}">{{$setting->email}}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="primary-footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <h5>درباره ما</h5>
                    <p class="mb-3"></p> <a class="btn-simple" href="{{route('contact-us')}}"><span>اطلاعات بیشتر <i class="mr-2 fas fa-long-arrow-alt-left"></i></span></a>
                </div>
                <div class="col-lg-3 col-md-6 sm-mt-5 footer-list">
                    <h5> خدمات</h5>
                    <ul class="list-unstyled">
                        <li><a href="#"><i class="fas fa-angle-left"></i>خدمات اول</a>
                        </li>

                    </ul>
                </div>
                <div class="col-lg-3 col-md-6 md-mt-5 widget">
                    <h5>آخرین مقالات</h5>
                    <div class="recent-post mb-0">
                        <ul class="list-unstyled">
                            @foreach($last_articles as $lArticle)
                            <li class="mb-3">
                                <div class="recent-post-thumb">
                                    @if(!$lArticle->Hasmedia('images'))
                                        <img class="img-fluid" src="{{asset('img/no-img.gif')}}" alt="{{$lArticle->title}}" title="{{$lArticle->title}}">

                                    @else
                                        <img class="img-fluid" src="{{$lArticle->getFirstMediaUrl('images')}}" alt="{{$lArticle->title}}" title="{{$lArticle->title}}">

                                    @endif

                                </div>
                                <div class="recent-post-desc"> <a href="#">{{$lArticle->title}}</a>
                                    <span>{{Morilog\Jalali\Jalalian::forge($lArticle->create_at)->format('%A, %d %B %y')}}</span>
                                </div>
                            </li>
                            @endforeach

                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 md-mt-5">
                    <h5> ساعات کاری</h5>

                    <ul class="list-unstyled working-hours clearfix">
                        <li><span>09:00 الی 17:00 </span>شنبه - جهارشنبه</li>
                        <li><span>10:00 الی 14:00 </span>پنجشنبه</li>
                        <li><span>تعطیل</span> جمعه</li>
                    </ul>
                    <div class="social-icons social-colored mt-3">
                        <ul class="list-inline mb-0">
                            <li class="social-facebook"><a href="https://telegram.me/{{$setting->info->telegram}}"><i class="fab fa-telegram" aria-hidden="true"></i></a>
                            </li>
                            <li class="social-twitter"><a href="https://wa.me/{{$setting->info->whatsapp}}"><i class="fab fa-whatsapp" aria-hidden="true"></i></a>
                            </li>
                            <li class="social-gplus"><a href="https://www.instagram.com/{{$setting->info->instagram}}"><i class="fab fa-instagram" aria-hidden="true"></i></a>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="secondary-footer">
        <div class="container">
            <div class="copyright">
                <div class="row align-items-center">
                    <div class="col-md-7">
                        <span>کپی رایت 2018 میستو. طراحی و توسعه توسط <a target="_blank" href="#"> علیرضا روشن  </a> | همه حقوق محفوظ است</span>
                    </div>

                </div>
            </div>
        </div>
    </div>
</footer>

<!--footer end-->


</div>

<!-- page wrapper end -->



<!--back-to-top start-->

<div class="scroll-top"><a class="smoothscroll" href="#top"><i class="fas fa-chevron-up"></i></a></div>

<!--back-to-top end-->


<!-- inject js start -->

<!--== jquery -->
<script src="{{asset('template/js/jquery.min.js')}}"></script>

<!--== popper -->
<script src="{{asset('template/js/popper.min.js')}}"></script>

<!--== bootstrap -->
<script src="{{asset('template/js/bootstrap.min.js')}}"></script>

<!--== appear -->
<script src="{{asset('template/js/jquery.appear.js')}}"></script>

<!--== modernizr -->
<script src="{{asset('template/js/modernizr.js')}}"></script>

<!--== menu -->
<script src="{{asset('template/js/menu/jquery.smartmenus.js')}}"></script>

<!--== audioplayer -->
<script src="{{asset('template/js/audioplayer/media-player.js')}}"></script>

<!--== magnific-popup -->
<script src="{{asset('template/js/magnific-popup/jquery.magnific-popup.min.js')}}"></script>

<!--== owl-carousel -->
<script src="{{asset('template/js/owl-carousel/owl.carousel.min.js')}}"></script>

<!--== parallax -->
<script src="{{asset('template/js/parallax/parallaxie.min.js')}}"></script>

<!--== counter -->
<script src="{{asset('template/js/counter/counter.js')}}"></script>

<!--== countdown -->
<script src="{{asset('template/js/countdown/jquery.countdown.min.js')}}"></script>

<!--== isotope -->
<script src="{{asset('template/js/isotope/isotope.pkgd.min.js')}}"></script>

<!--== contact-form -->
<script src="{{asset('template/js/contact-form/contact-form.js')}}"></script>

<!--== validate -->
<script src="{{asset('template/js/contact-form/jquery.validate.min.js')}}"></script>

<!--== particle -->
<script src="{{asset('template/js/particle/jquery.particleground.min.js')}}"></script>

<!--== theme-script -->
<script src="{{asset('template/js/theme-script.js')}}"></script>

@yield('scripts')

<!-- inject js end -->

</body>

</html>
