<?php

namespace Modules\Seo\Entities;

use Illuminate\Database\Eloquent\Model;

class Seo extends Model
{
    protected $fillable = ['title','description','keyword','seoable_id','seoable_type','robots','canonical','author','publisher'];

    public function seoable()
    {
        return $this->morphTo();
    }
}
