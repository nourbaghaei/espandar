<?php

namespace Modules\Educational\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Educational\Entities\ClassRoom;
use Modules\Educational\Entities\Race;
use Spatie\Permission\Models\Permission;

class PermissionsTableSeederTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('permissions')->whereModel(ClassRoom::class)->delete();

        Permission::create(['name'=>'classroom-list','model'=>ClassRoom::class,'created_at'=>now()]);
        Permission::create(['name'=>'classroom-create','model'=>ClassRoom::class,'created_at'=>now()]);
        Permission::create(['name'=>'classroom-edit','model'=>ClassRoom::class,'created_at'=>now()]);
        Permission::create(['name'=>'classroom-delete','model'=>ClassRoom::class,'created_at'=>now()]);


        DB::table('permissions')->whereModel(Race::class)->delete();

        Permission::create(['name'=>'race-list','model'=>Race::class,'created_at'=>now()]);
        Permission::create(['name'=>'race-create','model'=>Race::class,'created_at'=>now()]);
        Permission::create(['name'=>'race-edit','model'=>Race::class,'created_at'=>now()]);
        Permission::create(['name'=>'race-delete','model'=>Race::class,'created_at'=>now()]);



    }
}
