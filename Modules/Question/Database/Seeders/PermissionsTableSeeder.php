<?php

namespace Modules\Question\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Question\Entities\Question;
use Spatie\Permission\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('permissions')->whereModel(Question::class)->delete();

        Permission::create(['name'=>'questions-list','model'=>Question::class,'created_at'=>now()]);
        Permission::create(['name'=>'questions-create','model'=>Question::class,'created_at'=>now()]);
        Permission::create(['name'=>'questions-edit','model'=>Question::class,'created_at'=>now()]);
        Permission::create(['name'=>'questions-delete','model'=>Question::class,'created_at'=>now()]);
    }
}
