@include('core::layout.modules.index',[

    'title'=>__('plan::plans.index'),
    'items'=>$items,
    'parent'=>'plan',
    'model'=>'plan',
    'directory'=>'plans',
    'collect'=>__('plan::plans.collect'),
    'singular'=>__('plan::plans.singular'),
    'create_route'=>['name'=>'plans.create'],
    'edit_route'=>['name'=>'plans.edit','name_param'=>'plan'],
    'destroy_route'=>['name'=>'plans.destroy','name_param'=>'plan'],
     'search_route'=>true,
     'question_route'=>true,
       'setting_route'=>true,
      'category_route'=>true,
    'datatable'=>[
    __('cms.title')=>'title',
    __('cms.slug')=>'slug',
     __('cms.status')=>'GetStatus',
    __('cms.price')=>'PriceFormat',
     __('cms.update_date')=>'AgoTimeUpdate',
    __('cms.create_date')=>'TimeCreate',
    ],
        'detail_data'=>[
    __('cms.title')=>'title',
    __('cms.slug')=>'slug',
     __('cms.status')=>'GetStatus',
    __('cms.price')=>'PriceFormat',
    __('cms.slug')=>'slug',
        __('cms.create_date')=>'created_at',
    __('cms.update_date')=>'updated_at',
    ],


])
