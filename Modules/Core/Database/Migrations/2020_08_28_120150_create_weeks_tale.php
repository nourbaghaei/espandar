<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWeeksTale extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weeks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('saturday',30)->nullable();
            $table->string('sunday',30)->nullable();
            $table->string('monday',30)->nullable();
            $table->string('tuesday',30)->nullable();
            $table->string('wednesday',30)->nullable();
            $table->string('thursday',30)->nullable();
            $table->string('friday',30)->nullable();
            $table->morphs('weekable');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weeks');
    }
}
