<!--hero section start-->

<section class="banner p-0 pos-r fullscreen-banner text-center">
    <div class="banner-slider owl-carousel no-pb" data-dots="false" data-nav="true">
        @foreach($sliders as $key=>$slider)
                             @if($slider->Hasmedia('images'))
                <div class="item" data-bg-img="{{$slider->getFirstMediaUrl('images')}}" data-overlay="6">

                             @else
                        <div class="item" data-bg-img="{{asset('template/images/1.jpg')}}" data-overlay="6">

                             @endif

            <div class="align-center">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-10 col-md-12 mr-auto ml-auto">
                            <div class="box-shadow px-5 xs-px-1 py-5 xs-py-2 banner-1 pos-r" data-bg-color="rgba(255,255,255,0.030)">
                                <h5 class="text-white mb-3 letter-space-3 animated6">{{$slider->text}}</h5>
                                <h1 class="text-white mb-3 animated8"> {{$slider->title}}</h1>
                                <p class="lead text-white animated5 mb-3">{{$slider->excerpt}}</p> <a class="btn btn-theme btn-radius animated7" href="#">بیشتر بدانید</a>
                                <a class="btn btn-border btn-radius animated6" href="{{route('contact-us')}}">تماس با ما</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            @endforeach

    </div>
</section>

<!--hero section end-->


{{--<section id="main-slider" class="carousel slide carousel-fade card" data-ride="carousel">--}}
{{--     <ol class="carousel-indicators">--}}
{{--         @foreach($sliders as $key=>$slider)--}}
{{--             @if($loop->first)--}}
{{--         <li data-target="#main-slider" data-slide-to="0" class="active"></li>--}}
{{--             @else--}}
{{--         <li data-target="#main-slider" data-slide-to="{{$key ++}}" ></li>--}}
{{--             @endif--}}
{{--          @endforeach--}}
{{--     </ol>--}}
{{--     <div class="carousel-inner">--}}
{{--         @foreach($sliders as $slider)--}}
{{--             @if($loop->first)--}}
{{--         <div class="carousel-item active">--}}
{{--             <a class="d-block" href="#">--}}
{{--                 @if($slider->Hasmedia('images'))--}}
{{--                 <img src="{{$slider->getFirstMediaUrl('images')}}"--}}
{{--                      class="d-block w-100" alt="">--}}
{{--                 @else--}}
{{--                     <img src="{{asset('template/images/1.jpg')}}"--}}
{{--                          class="d-block w-100" alt="">--}}
{{--                 @endif--}}
{{--             </a>--}}
{{--         </div>--}}
{{--             @else--}}
{{--         <div class="carousel-item">--}}
{{--             <a class="d-block" href="#">--}}
{{--                 @if($slider->Hasmedia('images'))--}}
{{--                     <img src="{{$slider->getFirstMediaUrl('images')}}"--}}
{{--                          class="d-block w-100" alt="">--}}
{{--                 @else--}}
{{--                     <img src="{{asset('template/images/1.jpg')}}"--}}
{{--                          class="d-block w-100" alt="">--}}
{{--                 @endif--}}
{{--             </a>--}}
{{--         </div>--}}
{{--             @endif--}}

{{--         @endforeach--}}
{{--     </div>--}}
{{--     <a class="carousel-control-prev" href="#main-slider" role="button" data-slide="prev">--}}
{{--         <i class="now-ui-icons arrows-1_minimal-right"></i>--}}
{{--     </a>--}}
{{--     <a class="carousel-control-next" href="#main-slider" data-slide="next">--}}
{{--         <i class="now-ui-icons arrows-1_minimal-left"></i>--}}
{{--     </a>--}}
{{-- </section>--}}
