@extends('template.app')


@section('content')

    <!-- main -->
    <main class="profile-user-page default">
        <div class="container">
            <div class="row">
                <div class="profile-page col-xl-9 col-lg-8 col-md-12 order-2">
                    @include('core::layout.alert-danger')
                    @include('core::layout.alert-success')
                    <div class="row">
                        <div class="col-12">
                            <div class="col-12">
                                <h1 class="title-tab-content">ویرایش اطلاعات شخصی</h1>
                            </div>
                            <div class="content-section default">
                                <div class="row">
                                    <div class="col-12">
                                        <h1 class="title-tab-content">حساب شخصی</h1>
                                    </div>
                                </div>
                                <form action="{{route('client.update',['client'=>$client])}}" method="POST" class="form-account">
                                    @csrf
                                    @method('PATCH')
                                    <div class="row">
                                        <div class="col-sm-12 col-md-6">
                                            <div class="form-account-title">نام</div>
                                            <div class="form-account-row">
                                                <input class="input-field text-right" name="first_name" type="text" value="{{$client->first_name ? $client->first_name  : old('first_name') }}" placeholder="نام خود را وارد نمایید" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-6">
                                            <div class="form-account-title">نام خانوادگی</div>
                                            <div class="form-account-row">
                                                <input class="input-field text-right" name="last_name" type="text" value="{{$client->last_name ? $client->last_name  : old('last_name') }}" placeholder="نام خانوادگی خود را وارد نمایید" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-6">
                                            <div class="form-account-title">کد ملی</div>
                                            <div class="form-account-row">
                                                <input class="input-field text-right" type="text" name="identity_card" value="{{$client->identity_card  ? $client->identity_card  : old('identity_card')}}" placeholder="کد ملی خود را وارد نمایید" autocomplete="off" maxlength="10">
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-6">
                                            <div class="form-account-title">نام کاربری </div>
                                            <div class="form-account-row">
                                                <input class="input-field text-right" name="username"  value="{{$client->username ? $client->username  : old('username')}}" type="text" placeholder="نام کاربری خود را وارد نمایید" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-6">
                                            <div class="form-account-title">آدرس ایمیل</div>
                                            <div class="form-account-row">
                                                <input class="input-field text-right" type="email" name="email" value="{{$client->email ? $client->email  : old('email')}}" placeholder=" آدرس ایمیل خود را وارد نمایید" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-6">
                                            <div class="form-account-title">آدرس </div>
                                            <div class="form-account-row">
                                                <textarea class="input-field text-right" type="text" name="address"  placeholder=" آدرس  خود را وارد نمایید" autocomplete="off">{{$client->address ? $client->address  : old('address')}}</textarea>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-6">
                                            <div class="form-account-title">کد پستی</div>
                                            <div class="form-account-row">
                                                <input class="input-field text-right" type="text" name="postal_code" value="{{$client->postal_code ? $client->postal_code  : old('postal_code')}}" placeholder=" کد پستی خود را وارد نمایید" autocomplete="off" maxlength="10">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12 text-center">
                                        <button class="btn btn-default btn-lg" type="submit">ذخیره</button>
                                        <a href="{{route('client.dashboard')}}" class="btn btn-default btn-lg">انصراف</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
               @include('template.auth.sections.short-information')
            </div>
        </div>
    </main>
    <!-- main -->

@endsection



