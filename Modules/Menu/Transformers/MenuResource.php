<?php

namespace Modules\Menu\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class MenuResource extends Resource
{
    public $preserveKeys = true;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'title'=>$this->title,
            'symbol'=>$this->symbol,
            'href'=>$this->href,
            'order'=>$this->order,
            'token'=>$this->token,
            'update_date'=>$this->AgoTimeUpdate,
            'create_date'=>$this->TimeCreate,
        ];
    }
}
