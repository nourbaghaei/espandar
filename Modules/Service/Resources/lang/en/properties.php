<?php
return [
    "text-create"=>"you can create your property",
    "text-edit"=>"you can edit your property",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "error"=>"error",
    "index"=>"properties list",
    "singular"=>"property",
    "collect"=>"properties",
    "permission"=>[
        "property-full-access"=>"properties full access",
        "property-list"=>"properties list",
        "property-delete"=>"property delete",
        "property-create"=>"property create",
        "property-edit"=>"edit property",
    ]
];
