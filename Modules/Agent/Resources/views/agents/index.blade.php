@include('core::layout.modules.index',[

    'title'=>__('agent::agents.index'),
    'items'=>$items,
    'parent'=>'agent',
    'model'=>'agent',
    'directory'=>'agents',
    'collect'=>__('agent::agents.collect'),
    'singular'=>__('agent::agents.singular'),
    'create_route'=>['name'=>'agents.create'],
    'edit_route'=>['name'=>'agents.edit','name_param'=>'agent'],
    'destroy_route'=>['name'=>'agents.destroy','name_param'=>'agent'],
    'search_route'=>true,
     'pagination'=>true,
    'datatable'=>[__('cms.thumbnail')=>'thumbnail',
    __('cms.first_name')=>'first_name',
    __('cms.last_name')=>'last_name',
    __('cms.email')=>'email',
    __('cms.branch')=>'branch',
    __('cms.mobile')=>'mobile',
    __('cms.role')=>'RoleName',
    __('cms.order')=>'order',
      __('cms.update_date')=>'AgoTimeUpdate',
    __('cms.create_date')=>'TimeCreate',
    ],
        'detail_data'=>[
            __('cms.thumbnail')=>'thumbnail',
    __('cms.first_name')=>'first_name',
    __('cms.last_name')=>'last_name',
    __('cms.email')=>'email',
     __('cms.branch')=>'branch',
    __('cms.role')=>'RoleName',
    __('cms.order')=>'order',
    __('cms.slug')=>'slug',
        __('cms.create_date')=>'created_at',
    __('cms.update_date')=>'updated_at',
    ],


])





