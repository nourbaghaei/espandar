
<div class="widget-services widget card">
    <div class="row">
        @foreach($properties as $property)
            @if ($loop->first)
                <div class="feature-item col-12">
                    <a href="#" target="_blank">
                        @if(!$property->Hasmedia('images'))--}}
                        <img src="{{asset('img/no-img.gif')}}">
                        @else
                            <img src="{{$property->getFirstMediaUrl('images')}}">
                        @endif
                    </a>
                    <p>{{$property->title}}</p>
                </div>
            @else

        <div class="feature-item col-6">
            <a href="#" target="_blank">
                @if(!$property->Hasmedia('images'))--}}
                    <img src="{{asset('img/no-img.gif')}}">
                @else
                    <img src="{{$property->getFirstMediaUrl('images')}}">
                @endif
            </a>
            <p>{{$property->title}}</p>
        </div>
            @endif

        @endforeach
    </div>
</div>

