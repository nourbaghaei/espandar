<?php

namespace Modules\Brand\Entities;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Helper\Trades\TimeAttribute;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Brand extends Model implements HasMedia
{
    use Sluggable,HasMediaTrait,TimeAttribute;

    protected $fillable = ['title','text','excerpt','token','slug','order','user','href','target'];

    public function getRouteKeyName()
    {
        return multiRouteKey();
    }

    /**
     * @inheritDoc
     */
    public function sluggable(): array
    {
        return [
            'slug'=>[
                'source'=>'title'
            ]
        ];
    }
}
