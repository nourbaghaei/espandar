<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Client\Entities\Client;
use Modules\Core\Helper\Trades\TimeAttribute;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class UserAction extends Model implements HasMedia
{

    use HasMediaTrait,TimeAttribute;

    protected $table="user_actions";

    protected $fillable = ['title','text','status','start_at','end_at','client','score','rate','record','actionable_type','actionable_id','award','payment'];

    public  function selfClient(){

        return $this->belongsTo(Client::class,'client','id');
    }

    public function student(){
        return $this->hasOne(Client::class,'id','client');
    }

}
