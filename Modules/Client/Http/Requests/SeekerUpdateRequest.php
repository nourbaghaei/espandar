<?php

namespace Modules\Client\Http\Requests;

use App\Rules\Nationalcode;
use Illuminate\Foundation\Http\FormRequest;

class SeekerUpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'=>'email|unique:clients,email,'.$this->seeker.',token',
            'mobile'=>'unique:clients,email,'.$this->seeker.',token',
            'password'=>'nullable|min:8',
            'first_name'=>'required|string',
            'last_name'=>'required|string',
            'identity_card' => ['nullable',new Nationalcode()]
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
