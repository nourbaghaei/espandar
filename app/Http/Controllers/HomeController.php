<?php

namespace App\Http\Controllers;

use App\Events\Visit;
use App\Facades\Rest\Rest;
use App\Http\Requests\RegisterReceiverRequest;
use App\Http\Requests\SendChat;
use Artesaos\SEOTools\Facades\SEOMeta;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Modules\Advertising\Entities\Advertising;
use Modules\Article\Entities\Article;
use Modules\Chat\Entities\Chat;
use Modules\Client\Entities\Client;
use Modules\Client\Http\Requests\AddAdvertEmployer;
use Modules\Client\Http\Requests\ChangePasswordRequest;
use Modules\Client\Http\Requests\ClientUpdateRequest;
use Modules\Client\Http\Requests\CvSendRequest;
use Modules\Client\Http\Requests\SeekerUpdateRequest;
use Modules\Core\Entities\Category;
use Modules\Core\Entities\City;
use Modules\Core\Entities\Country;
use Modules\Core\Entities\Currency;
use Modules\Core\Entities\Setting;
use Modules\Core\Entities\State;
use Modules\Core\Entities\UserAction;
use Modules\Core\Helper\CoreHelper;
use Modules\Educational\Entities\ClassRoom;
use Modules\Educational\Entities\Race;
use Modules\Event\Entities\Event;
use Modules\Gallery\Entities\Gallery;
use Modules\Information\Entities\Information;
use Modules\Member\Entities\Member;
use Modules\Page\Entities\Page;
use Modules\Payment\Http\Controllers\Geteway\Melat;
use Modules\Payment\Http\Controllers\Geteway\ZarinPal;
use Modules\Plan\Entities\Plan;
use Modules\Portfolio\Entities\Portfolio;
use Modules\Product\Entities\Cart;
use Modules\Product\Entities\CartList;
use Modules\Product\Entities\Product;
use Modules\Request\Entities\listRequest;
use Modules\Request\Http\Requests\RequestRequest;
use Modules\Service\Entities\Advantage;
use Modules\Service\Entities\Property;
use Modules\Sms\Http\Controllers\Gateway\Idepardazan;
use Modules\Sms\Http\Controllers\Gateway\Kavenegar;
use Modules\Video\Entities\Video;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\Tags\Url;
use Spatie\Tags\Tag;


class HomeController extends Controller
{
    public function resetPasswordProfile($object,$current,$pass){

        if(Hash::check($current, $object->password)){

            $this->resetPassword($object,$pass);

            return $errorStatus=true;
        }
        else{
            return $errorStatus=false;
        }
    }
    protected function resetPassword($user, $password)
    {
        $this->setUserPassword($user, $password);

        $user->save();
    }

    protected function setUserPassword($user, $password)
    {
        $user->password = Hash::make($password);
    }

    public function generateSiteMap(){
        try {
            app()->make(\Spatie\Permission\PermissionRegistrar::class)->forgetCachedPermissions();


            Artisan::call('cache:clear');

            $sitemap=Sitemap::create();

            $sitemap->add(Url::create('/')
                ->setLastModificationDate(Carbon::yesterday())
                ->setChangeFrequency(Url::CHANGE_FREQUENCY_YEARLY)
                ->setPriority(0.2));

            $sitemap->add(Url::create('/about-us')
                ->setLastModificationDate(Carbon::yesterday())
                ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
                ->setPriority(0.2));
            $sitemap->add(Url::create('/contact-us')
                ->setLastModificationDate(Carbon::yesterday())
                ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
                ->setPriority(0.2));

            if(CoreHelper::hasCourse()){
                $sitemap->add(Url::create('/courses')
                    ->setLastModificationDate(Carbon::yesterday())
                    ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
                    ->setPriority(0.2));
            }

            if(CoreHelper::hasEvent()){
                $sitemap->add(Url::create('/events')
                    ->setLastModificationDate(Carbon::yesterday())
                    ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
                    ->setPriority(0.2));
            }

            if(CoreHelper::hasPortfolio()){
                $sitemap->add(Url::create('/portfolios')
                    ->setLastModificationDate(Carbon::yesterday())
                    ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
                    ->setPriority(0.2));
            }
            if(CoreHelper::hasInformation()){
                $sitemap->add(Url::create('/informations')
                    ->setLastModificationDate(Carbon::yesterday())
                    ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
                    ->setPriority(0.2));
            }
            if(CoreHelper::hasAdvertising()){
                $sitemap->add(Url::create('/advertisings')
                    ->setLastModificationDate(Carbon::yesterday())
                    ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
                    ->setPriority(0.2));
            }
            if(CoreHelper::hasPortfolio()){
                $sitemap->add(Url::create('/advertisings')
                    ->setLastModificationDate(Carbon::yesterday())
                    ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
                    ->setPriority(0.2));
            }

            if(CoreHelper::hasProduct()){
                $sitemap->add(Url::create('/products')
                    ->setLastModificationDate(Carbon::yesterday())
                    ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
                    ->setPriority(0.2));
            }




            if(CoreHelper::hasRace()) {
                $sitemap->add(Url::create('/races')
                    ->setLastModificationDate(Carbon::yesterday())
                    ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
                    ->setPriority(0.2));

            }

            if(CoreHelper::hasArticle()) {
                foreach (Article::latest()->get() as $article) {
                    $sitemap->add(Url::create('/articles/' . $article->slug)
                        ->setLastModificationDate(Carbon::yesterday())
                        ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
                        ->setPriority(0.2));
                }
            }
            if(CoreHelper::hasAdvertising()) {
                foreach (Advertising::latest()->get() as $advertising) {
                    $sitemap->add(Url::create('/advertisings/' . $advertising->slug)
                        ->setLastModificationDate(Carbon::yesterday())
                        ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
                        ->setPriority(0.2));
                }
            }
            if(CoreHelper::hasInformation()) {
                foreach (Advertising::latest()->get() as $information) {
                    $sitemap->add(Url::create('/informations/' . $information->slug)
                        ->setLastModificationDate(Carbon::yesterday())
                        ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
                        ->setPriority(0.2));
                }
            }

            if(CoreHelper::hasPortfolio()) {
                foreach (Portfolio::latest()->get() as $portfolio) {
                    $sitemap->add(Url::create('/portfolios/' . $portfolio->slug)
                        ->setLastModificationDate(Carbon::yesterday())
                        ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
                        ->setPriority(0.2));
                }
            }

            if(CoreHelper::hasProduct()) {
                foreach (Product::latest()->get() as $product) {
                    $sitemap->add(Url::create('/products/' . $product->slug)
                        ->setLastModificationDate(Carbon::yesterday())
                        ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
                        ->setPriority(0.2));
                }
            }

            if(CoreHelper::hasCourse()) {
                foreach (ClassRoom::latest()->get() as $classroom) {
                    $sitemap->add(Url::create('/courses/' . $classroom->slug)
                        ->setLastModificationDate(Carbon::yesterday())
                        ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
                        ->setPriority(0.2));
                }
            }

            if(CoreHelper::hasRace()) {
                foreach (Race::latest()->get() as $race) {
                    $sitemap->add(Url::create('/races/' . $race->slug)
                        ->setLastModificationDate(Carbon::yesterday())
                        ->setChangeFrequency(Url::CHANGE_FREQUENCY_WEEKLY)
                        ->setPriority(0.2));
                }
            }

            $sitemap->writeToFile(public_path('sitemap.xml'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }

    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function website()
    {

        try {
            $setting=Setting::with('info')->firstOrFail();
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle($setting->seo->title);
            SEOMeta::setDescription($setting->seo->description);
            SEOMeta::setCanonical(env('APP_URL'));
            return view('template.index');
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }

    public function articles(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('articles');
            $items=Article::latest()->with('user_info')->get();
            $categories=Category::whereModel(Article::class)->whereStatus(1)->whereParent(0)->get();
            SEOMeta::setDescription("");
            SEOMeta::setKeywords("");
            SEOMeta::setCanonical(env('APP_URL').'/articles');
            return view('template.articles.index',compact('items','categories'));

        }catch (\Exception $exception){

            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function sendChat(SendChat $request){
        try {

            $chat=Chat::create([
                'client'=>auth('client')->user()->id,
                'text'=>$request->message,
                'created_at'=>now(),
            ]);
            if(!$chat){
                return redirect()->back()->with('error',__('client::clients.error'));

            }else{
                return redirect()->back()->with('message',__('client::clients.store'));
            }
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function students(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('students');
            $items=ClassRoom::orderBy('order','asc')->whereParent(0)->latest()->get();
            SEOMeta::setDescription("");
            SEOMeta::setKeywords("");
            SEOMeta::setCanonical(env('APP_URL').'/students');
            return view('template.students.index',compact('items'));

        }catch (\Exception $exception){

            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function singleStudents($id){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('students');
            $class=ClassRoom::orderBy('order','asc')->findOrFail($id);
            $items=UserAction::with('selfClient')->where('actionable_type',ClassRoom::class)->where('actionable_id',$class->id)->where('status',1)->get();
            SEOMeta::setDescription("");
            SEOMeta::setKeywords("");
            SEOMeta::setCanonical(env('APP_URL').'/students/single');
            return view('template.students.single',compact('items'));

        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function galleries(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('تصاویر');
            $categories=Category::orderBy('order','asc')->whereModel(Gallery::class)->whereStatus(1)->whereParent(0)->get();
            SEOMeta::setDescription("");
            SEOMeta::setKeywords("");
            SEOMeta::setCanonical(env('APP_URL').'/galleries');
            return view('template.galleries.index',compact('categories'));

        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function singleGallery($category){

        try {
            $item=Category::find($category);
            $items=Gallery::whereCategory($category)->get();
            SEOMeta::setTitleDefault(\setting('name'));
            event(new Visit($item));
            return view('template.galleries.single',compact('items'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function videos(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('فیلم ها');
            $categories=Category::orderBy('order','asc')->whereModel(Video::class)->whereStatus(1)->whereParent(0)->get();
            SEOMeta::setDescription("");
            SEOMeta::setKeywords("");
            SEOMeta::setCanonical(env('APP_URL').'/videos');
            return view('template.videos.index',compact('categories'));

        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function singleVideo($category){

        try {
            $items=Video::whereCategory($category)->get();
            SEOMeta::setTitleDefault(\setting('name'));
            return view('template.videos.single',compact('items'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function articlesTag($tag){
        try {
            $tag_id=Tag::whereJsonContains('name',['fa'=>$tag])->first()->id;
            $articles=Article::with('user_info')->get();
            $items=null;
            foreach ($articles as $article){

                foreach ($article->tags as $tag){
                    if($tag->id==$tag_id){
                        $items[]=$article;

                    }
                }

            }
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('مقالات برچسب شده');
            SEOMeta::setDescription("");
            SEOMeta::setKeywords("");
            SEOMeta::setCanonical(env('APP_URL').'/articles/tag/'.$tag);
            return view('template.articles.index',compact('items'));

        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function articlesCategory($category){
        try {
            $category=Category::whereSlug($category)->first()->id;
            $articles=Article::with('user_info')->get();
            $items=null;
            foreach ($articles as $article){
                if($article->category==$category){
                    $items[]=$article;
                }

            }
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('مقالات دسته بندی شده');
            SEOMeta::setDescription("");
            SEOMeta::setKeywords("");
            SEOMeta::setCanonical(env('APP_URL').'/articles/category/'.$category);
            return view('template.articles.index',compact('items'));

        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function courses(){
        try {
            SEOMeta::setTitle('کلاس ها');
            $items=ClassRoom::with('member','price','analyzer')->has('member')->orderBy('order','asc')->whereParent(0)->latest()->get();
            SEOMeta::setDescription("");
            SEOMeta::setKeywords("");
            SEOMeta::setCanonical(env('APP_URL').'/courses');
            return view('template.courses.index',compact('items'));

        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return  abort('500');
        }
    }
    public function races(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('مسابقات');
            $items=Race::with('member','price','analyzer')->has('member')->orderBy('order','asc')->whereParent(0)->latest()->get();
            SEOMeta::setDescription("");
            SEOMeta::setKeywords("");
            SEOMeta::setCanonical(env('APP_URL').'/races');
            return view('template.races.index',compact('items'));

        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }

    public function singleArticle($obj){

        try {
            $item=Article::with('user_info')->whereSlug($obj)->firstOrFail();
            $categories=Category::whereModel(Article::class)->whereStatus(1)->whereParent(0)->get();
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle($item->seo->title);
            SEOMeta::setDescription($item->seo->description);
            SEOMeta::setKeywords($item->seo->keyword);
            SEOMeta::setCanonical($item->seo->canonical);
            event(new Visit($item));
            return view('template.articles.single',compact('item','categories'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function singleEvent($obj){

        try {
            $item=Event::whereSlug($obj)->firstOrFail();
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle($item->seo->title);
            SEOMeta::setDescription($item->seo->description);
            SEOMeta::setKeywords($item->seo->keyword);
            SEOMeta::setCanonical($item->seo->canonical);
            event(new Visit($item));
            return view('template.events.single',compact('item'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }

    public function singleCourse($obj){

        try {
            $item=ClassRoom::with('member','price','analyzer','students')->has('member')->whereSlug($obj)->firstOrFail();
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle($item->seo->title);
            SEOMeta::setDescription($item->seo->description);
            SEOMeta::setKeywords($item->seo->keyword);
            SEOMeta::setCanonical($item->seo->canonical);
            event(new Visit($item));
            return view('template.courses.single',compact('item'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }

    public function singleRace($obj){

        try {
            $item=Race::with('member','price','analyzer','students')->has('member')->whereSlug($obj)->firstOrFail();
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle($item->seo->title);
            SEOMeta::setDescription($item->seo->description);
            SEOMeta::setKeywords($item->seo->keyword);
            SEOMeta::setCanonical($item->seo->canonical);
            event(new Visit($item));
            return view('template.races.single',compact('item'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function informations(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('اخبار');
            $items=Information::latest()->get();
            $categories=Category::whereModel(Information::class)->whereStatus(1)->whereParent(0)->get();
            SEOMeta::setCanonical(env('APP_URL').'/informations');
            return view('template.informations.index',compact('items','categories'));

        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function companies(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('شرکت کارفرمایان');
            $items=Client::with('company')->latest()
                ->whereHas('role',function ($query){
                    $query->where('title', '=', 'employer');
                })->get();
            SEOMeta::setCanonical(env('APP_URL').'/companies');
            return view('template.companies.index',compact('items'));

        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }

    public function singleInformation($obj){

        try {
            $item=Information::whereSlug($obj)->firstOrFail();
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle($item->seo->title);
            SEOMeta::setDescription($item->seo->description);
            SEOMeta::setKeywords($item->seo->keyword);
            SEOMeta::setCanonical($item->seo->canonical);
            event(new Visit($item));
            return view('template.informations.single',compact('item'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function singleCompany($obj){

        try {
//                $active_adverts=!is_null(Advertising::latest()->with('info_company')->whereStatus(2)->whereClient($obj)->get()) ? Advertising::latest()->with('info_company')->whereStatus(2)->whereClient($obj)->get() : [];
//
            $item=Client::with('company','analyzer','seo')->whereId($obj)
                ->whereHas('role',function ($query){
                    $query->where('title', '=', 'employer');
                })->first();
//            SEOMeta::setTitleDefault(\setting('name'));
//            SEOMeta::setTitle($item->seo->title);
//            SEOMeta::setDescription($item->seo->description);
//            SEOMeta::setKeywords($item->seo->keyword);
//            SEOMeta::setCanonical($item->seo->canonical);
//            event(new Visit($item));
//            return view('template.companies.single',compact('item','active_adverts'));
            return view('template.companies.single',compact('item'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }


    public function singleAdvertising($obj){

        try {
            $item=Advertising::with('info_company','client_info','analyzer')->whereStatus(2)->findOrFail($obj);
            $similar_advertisings=Advertising::where('category',$item->category)->where('id','!=',$obj)->get();
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle($item->seo->title);
            SEOMeta::setDescription($item->seo->description);
            SEOMeta::setKeywords($item->seo->keyword);
            SEOMeta::setCanonical($item->seo->canonical);
            event(new Visit($item));
            return view('template.advertisings.single',compact('item','similar_advertisings'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function addAdvert(AddAdvertEmployer $request){

        try {
            $client=Client::findOrFail(\auth('client')->user()->id);
            $category=Category::whereToken($request->input('category'))->firstOrFail();
            $currency=Currency::whereToken($request->input('currency'))->firstOrFail();
            $plan=getPlan();

            if($plan->status==2){
                return redirect()->back()->with('error','اعبار پلن شما به پایان رسیده است.');
            }
            $store=Advertising::create([
                'title'=>$request->input('title'),
                'excerpt'=>$request->input('excerpt'),
                'number_employees'=>$request->input('number_employees'),
                'category'=>$category->id,
                'salary'=>$request->input('salary'),
                'work_experience'=>$request->input('experience'),
                'job_position'=>json_encode($request->input('positions')),
                'education'=>json_encode($request->input('educations')),
                'guild'=>$request->input('guild'),
                'r'=>$request->input('kind'),
                'skill'=>json_encode($request->input('skills')),
                'kind'=>json_encode($request->input('kinds')),
                'expire'=>now()->addDays(Plan::find($plan->plan)->time_limit),
                'currency'=>$currency->id,
                'slug'=>null,
                'gender'=>$request->input('gender'),
                'plan'=>$plan->id,
                'text'=>$request->input('text'),
                'token'=>tokenGenerate(),
                'client'=>$client->id,
                'country'=>'Iran',
                'city'=>'Tehran',
                'state'=>'Tehran',
            ]);

            if($request->has('image')){
                destroyMedia($store,config('cms.collection-image'));
                $store->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

            $store->seo()->create();

            $store->analyzer()->create();

            $store->syncTags([]);

            $plan->update([
                'count'=>$plan->count -1
            ]);

            if($plan->count ==0){
                    $plan->update([
                        'status'=>2
                    ]);
            }

            if(!$store){
                return redirect()->back()->with('error',__('advertising::advertisings.error'));
            }else{
                return redirect()->back()->with('message',__('advertising::advertisings.store'));
            }


        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function advertisings(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('آگهی ها');
            $items=Advertising::latest()->with('client_info')->whereStatus(2)->latest()->paginate(config('cms.paginate'));
            SEOMeta::setCanonical(env('APP_URL').'/advertisings');
            return view('template.advertisings.index',compact('items'));

        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function portfolios(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('portfolios');
            $items=Portfolio::latest()->get();
            SEOMeta::setCanonical(env('APP_URL').'/portfolios');
            return view('template.portfolios.index',compact('items'));

        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }

    public function singlePortfolio($obj){

        try {
            $item=Portfolio::whereSlug($obj)->firstOrFail();
            $medias= $item->getMedia(config('cms.collection-images'));
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle($item->seo->title);
            SEOMeta::setDescription($item->seo->description);
            SEOMeta::setKeywords($item->seo->keyword);
            SEOMeta::setCanonical($item->seo->canonical);
            event(new Visit($item));
            return view('template.portfolios.single',compact('item','medias'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function singleProperty($obj){

        try {
            $item=Property::whereSlug($obj)->firstOrFail();
            SEOMeta::setTitleDefault(\setting('name'));
            return view('template.properties.single',compact('item'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function singleAdvantage($obj){

        try {
            $item=Advantage::whereSlug($obj)->firstOrFail();
            SEOMeta::setTitleDefault(\setting('name'));
            return view('template.advantages.single',compact('item'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }

    public function products(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('products');
            $items=Product::latest()->get();
            SEOMeta::setCanonical(env('APP_URL').'/products');
            return view('template.products.index',compact('items'));

        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function shop(){
        try {
            $items=Product::with('user_info')->whereIn('status',[1,3])->paginate(config('cms.paginate'));
            $categories=Category::whereModel(Product::class)->whereStatus(1)->whereParent(0)->get();
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('فروشگاه');
            SEOMeta::setCanonical(env('APP_URL').'/shop');
            return view('template.shop.index',compact('items','categories'));

        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function downloads(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('downloads');
            SEOMeta::setCanonical(env('APP_URL').'/downloads');
            return view('template.downloads.index');

        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function categoriesProduct(Request $request, $category){
        try {
            $item=Category::whereSlug($category)->first();

            $product_categories=Product::whereCategory($item->id)->get();

            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('categories');
            SEOMeta::setCanonical(env('APP_URL').'/categories');
            return view('template.categories.products.index',compact('product_categories'));

        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function events(){
        try {
            $items=Event::latest()->get();
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('events');
            SEOMeta::setCanonical(env('APP_URL').'/events');
            return view('template.events.index',compact('items'));

        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }

    public function singleProduct($obj){

        try {
            $item=Product::with('attributes','properties')->whereSlug($obj)->firstOrFail();
            $medias= $item->getMedia(config('cms.collection-images'));
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle($item->seo->title);
            SEOMeta::setDescription($item->seo->description);
            SEOMeta::setKeywords($item->seo->keyword);
            SEOMeta::setCanonical($item->seo->canonical);
            event(new Visit($item));
            return view('template.products.single',compact('item','medias'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function searchProduct($obj){

        try {
            $category=Category::find($obj);
            $items=is_null(Product::with('price')->whereCategory($category->id)->get()) ? [] :Product::with('price')->whereCategory($category->id)->get();
            $count=is_null(Product::with('price')->whereCategory($category->id)) ? 0 : Product::with('price')->whereCategory($category->id)->count();
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('جستجوی محصول');
            SEOMeta::setDescription('جستجوی محصول');
            SEOMeta::setKeywords('جستجوی آمحصول');
            return view('template.products.search',compact('items','count'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function showShopping(){

        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(' تکمیل سفارش ');
            SEOMeta::setDescription('تکمیل سفارش');
            SEOMeta::setKeywords('تکمیل سفارش');
            return view('template.auth.shopping');
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function showPayment(){

        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(' پرداخت سفارش ');
            SEOMeta::setDescription('پرداخت سفارش');
            SEOMeta::setKeywords('پرداخت سفارش');
            return view('template.auth.payment');
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }


    public function showOrders(){

        try {
            $client=Client::with('info','myCode','classrooms','races','adverts','plan','sendCV','wallet')->find(\auth('client')->user()->id);
            $items=null;
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(' لیست سفارشات ');
            SEOMeta::setDescription('لیست سفارشات');
            SEOMeta::setKeywords('لیست سفارشات');
            return view('template.auth.orders',compact('items','client'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function showCart(){

        try {
            $client=Client::with('info','myCode','classrooms','races','adverts','plan','sendCV','wallet')->find(\auth('client')->user()->id);
            $items=null;
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('  سبد خرید ');
            SEOMeta::setDescription('سبد خرید');
            SEOMeta::setKeywords('سبد خرید');
            return view('template.auth.cart',compact('items','client'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function showOrdersReturn(){

        try {
            $client=Client::with('info','myCode','classrooms','races','adverts','plan','sendCV','wallet')->find(\auth('client')->user()->id);
            $items=null;
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(' لیست سفارشات مرجوعی ');
            SEOMeta::setDescription('لیست سفارشات مرجوعی');
            SEOMeta::setKeywords('لیست سفارشات مرجوعی');
            return view('template.auth.orders_return',compact('items','client'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function singleMember($obj){

        try {
            $item=Member::whereSlug($obj)->firstOrFail();
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle($item->seo->title);
            SEOMeta::setDescription($item->seo->description);
            SEOMeta::setKeywords($item->seo->keyword);
            SEOMeta::setCanonical($item->seo->canonical);
            return view('template.members.single',compact('item'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function register(){

        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('ثبت نام');
            SEOMeta::setDescription('register');
            SEOMeta::setKeywords('register');
            SEOMeta::setCanonical(env('APP_URL').'/user/register');
            return view('template.auth.register.index',compact('item'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function login(){

        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('ورود');
            SEOMeta::setDescription('login');
            SEOMeta::setKeywords('login');
            SEOMeta::setCanonical(env('APP_URL').'/user/login');
            return view('template.auth.login',compact('item'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }

    public  function contactUs(){

        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('تماس با ما');
            SEOMeta::setDescription('تماس با ما');
            SEOMeta::setKeywords(' تماس با ما');
            SEOMeta::setCanonical(env('APP_URL').'/contact-us');
            return view('template.pages.contact_us',compact('item'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public  function aboutUs(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('درباره ما');
            SEOMeta::setDescription('درباره ما');
            SEOMeta::setKeywords('درباره ما');
            SEOMeta::setCanonical(env('APP_URL').'/about-us');
            return view('template.pages.about_us',compact('item'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }

    public  function categoryAdvertising(){
        try {
            $items=Category::with('advertisings')->orderBy('order','desc')->where('model',Advertising::class)->whereParent(0)->get();
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('دسته بندی آگهی ها');
            SEOMeta::setDescription('دسته بندی آگهی ها');
            SEOMeta::setKeywords('دسته بندی آگهی ها');
            SEOMeta::setCanonical(env('APP_URL').'/category-advertising');
            return view('template.pages.category_advertising',compact('items'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }

    public  function page($page){
        try {
            $page = Page::whereSlug($page)->firstOrFail();
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle($page->seo->title);
            SEOMeta::setDescription($page->seo->description);
            SEOMeta::setKeywords($page->seo->keyword);
            SEOMeta::setCanonical($page->seo->canonical);
            return view('template.pages.empty_page', compact('page'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }

    public  function logout(Request $request){

        if(auth('client')->check()){
            $data=[
                'auth'=>\auth('client')->user()
            ];
            sendCustomEmail($data,'emails.front.logout');
            auth('client')->logout();
        }
        return redirect(route('front.website'));
    }

    public function sendRequestMale(RequestRequest $request){

        try {



            $saved=\Modules\Request\Entities\Request::create([
                'email'=>$request->input('email'),
                'name'=>$request->input('name'),
                'phone'=>$request->input('phone'),
                'country'=>'german',
                'gender'=>'male',
                'loss'=>$request->input('ihr'),
                'color'=>$request->input('blond'),
                'fall_time'=>$request->input('Jahr'),
                'transplantation'=>$request->input('eine'),
                'feeling'=>$request->input('schlimm'),
                'execution_time'=>$request->input('behandlung'),
                'list_requests'=>listRequest::whereName('hair-transplantation')->firstOrFail()->id,
                'token'=>tokenGenerate(),
            ]);
            if($request->has('Vorne')){
                $saved->addMedia($request->file('Vorne'))->toMediaCollection('Vorne');

            }
            if($request->has('Hinten')){
                $saved->addMedia($request->file('Hinten'))->toMediaCollection('Hinten');

            }
            if($request->has('Oben')){
                $saved->addMedia($request->file('Oben'))->toMediaCollection('Oben');

            }


            if(!$saved){
                return redirect(route('request.hair'))->with('error',__('request::requests.error'));
            }else{
                $data=[
                    'gender'=>'Männlich',
                    'name'=>$request->input('name'),
                    'email'=>$request->input('email'),
                    'phone'=>$request->input('phone'),
                    'ihr'=>$request->input('ihr'),
                    'blond'=>$request->input('blond'),
                    'Jahr'=>$request->input('Jahr'),
                    'eine'=>$request->input('eine'),
                    'schlimm'=>$request->input('schlimm'),
                    'behandlung'=>$request->input('behandlung'),
                ];
                $files=[
                    'Vorne'=>$saved->getFirstMediaUrl('Vorne'),
                    'Hinten'=>$saved->getFirstMediaUrl('Hinten'),
                    'Oben'=>$saved->getFirstMediaUrl('Oben')

                ];
                $this->sendEmail($data,$files);
                return redirect(route("request.hair"))->with('message',__('request::requests.store'));
            }
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function sendRequestFemale(RequestRequest $request){

        try {


            $saved=\Modules\Request\Entities\Request::create([
                'email'=>$request->input('email'),
                'name'=>$request->input('name'),
                'phone'=>$request->input('phone'),
                'country'=>'german',
                'gender'=>'female',
                'loss'=>$request->input('ihr'),
                'color'=>$request->input('blond'),
                'fall_time'=>$request->input('Jahr'),
                'transplantation'=>$request->input('eine'),
                'feeling'=>$request->input('schlimm'),
                'execution_time'=>$request->input('behandlung'),
                'list_requests'=>listRequest::whereName('hair-transplantation')->firstOrFail()->id,
                'token'=>tokenGenerate(),
            ]);

            if($request->has('Vorne')){
                $saved->addMedia($request->file('Vorne'))->toMediaCollection('Vorne');
            }
            if($request->has('Hinten')){
                $saved->addMedia($request->file('Hinten'))->toMediaCollection('Hinten');
            }
            if($request->has('Oben')){
                $saved->addMedia($request->file('Oben'))->toMediaCollection('Oben');
            }

            if(!$saved){
                return redirect(route('request.hair'))->with('error',__('request::requests.error'));
            }else{


                $data=[
                    'gender'=>'Weiblich',
                    'name'=>$request->input('name'),
                    'email'=>$request->input('email'),
                    'phone'=>$request->input('phone'),
                    'ihr'=>$request->input('ihr'),
                    'blond'=>$request->input('blond'),
                    'Jahr'=>$request->input('Jahr'),
                    'eine'=>$request->input('eine'),
                    'schlimm'=>$request->input('schlimm'),
                    'behandlung'=>$request->input('behandlung'),
                ];
                $files=[
                    'Vorne'=>$saved->getFirstMediaUrl('Vorne'),
                    'Hinten'=>$saved->getFirstMediaUrl('Hinten'),
                    'Oben'=>$saved->getFirstMediaUrl('Oben')

                ];
                $this->sendEmail($data,$files);
                return redirect(route("request.hair"))->with('message',__('request::requests.store'));
            }
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function maleHair(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('male hair');
            SEOMeta::setDescription("");
            SEOMeta::setKeywords("");
            SEOMeta::setCanonical(env('APP_URL').'/hair/male');
            return view('template.forms.male_hair');

        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function resultHair(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('result hair');
            SEOMeta::setDescription("");
            SEOMeta::setKeywords("");
            SEOMeta::setCanonical(env('APP_URL').'/hair/result');
            return view('template.forms.result');

        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }

    public function panel(){

        try {
            $client=Client::with('info','myCode','classrooms','races','adverts','plan','sendCV','wallet','cart')->find(\auth('client')->user()->id);
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('پنل کاربری');
            SEOMeta::setDescription('پنل کاربری');
            SEOMeta::setKeywords('پنل کاربری');
            SEOMeta::setCanonical(env('APP_URL').'/user/panel');
            return view('template.auth.panel',compact('client'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');

        }
    }
    public function welcome(){

        try {
            $client=Client::with('info','myCode','classrooms','races','adverts','plan','sendCV','wallet','cart')->find(\auth('client')->user()->id);
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('خوش آمدید ا');
            SEOMeta::setDescription('خوش آمدید ');
            SEOMeta::setKeywords('خوش آمدید ');
            SEOMeta::setCanonical(env('APP_URL').'/user/panel/welcome');
            return view('template.auth.welcome',compact('client'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');

        }
    }

    public function showChangePassword(){

        try {

            $client=Client::with('info','myCode','classrooms','races','adverts','plan','sendCV','wallet')->find(\auth('client')->user()->id);
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('تغییر پسورد');
            SEOMeta::setDescription('تغییر پسورد');
            SEOMeta::setKeywords('تغییر پسورد');
            SEOMeta::setCanonical(env('APP_URL').'/user/panel/change/password/secret');
            return view('template.auth.change-password',compact('client'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');

        }
    }
    public function changePassword(ChangePasswordRequest $request){

        try {

            $client=Client::with('info','myCode','classrooms','races','adverts','plan','sendCV','wallet')->find(\auth('client')->user()->id);

            if(!empty(trim($request->input('current_password'))) && !empty(trim($request->input('new_password')))  ){

                if(!$this->resetPasswordProfile($client,$request->input('current_password'),$request->input('new_password'))){
                    return redirect()->back()->with('error',__('user::users.error'));
                }

            }
            return redirect()->route('client.dashboard');

        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');

        }
    }

    public function favorite(){
        try {
            $client=Client::with('info','myCode','classrooms','races','adverts','plan','sendCV','wallet')->find(\auth('client')->user()->id);
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('لیست علاقه مندی ها');
            SEOMeta::setDescription('لیست علاقه مندی ها');
            SEOMeta::setKeywords('لیست علاقه مندی ها');
            SEOMeta::setCanonical(env('APP_URL').'/user/panel/favorite');
            return view('template.auth.favorite',compact('client'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function cvs(){
        try {
            $client=Client::with('info','myCode','classrooms','races','adverts','plan','sendCV','wallet')->find(\auth('client')->user()->id);
            $advertisings=Advertising::whereClient(auth('client')->id())->orderBy('created_at','desc')->whereStatus(2)->pluck('id')->toArray();
            $actions=UserAction::with('selfClient')->whereIn('actionable_id',$advertisings)->where('actionable_type',Advertising::class)->get();
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('لیست رزومه ها');
            SEOMeta::setDescription('لیست رزومه ها');
            SEOMeta::setKeywords('لیست رزومه ها');
            SEOMeta::setCanonical(env('APP_URL').'/user/panel/cvs');
            return view('template.auth.cvs',compact('client','cvs','actions'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function plans(){
        try {
            $items=Plan::orderBy('order','desc')->get();
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(' پلن ها');
            SEOMeta::setDescription('   پلن ها');
            SEOMeta::setKeywords(' پلن ها');
            SEOMeta::setCanonical(env('APP_URL').'/plans');
            return view('template.plans.index',compact('items'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function clientPlans(){
        try {
            $client=Client::with('info','myCode','classrooms','races','adverts','plan','sendCV','wallet')->find(\auth('client')->user()->id);
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('لیست پلن ها');
            SEOMeta::setDescription('لیست پلن ها');
            SEOMeta::setKeywords('لیست پلن ها');
            SEOMeta::setCanonical(env('APP_URL').'/user/panel/cvs');
            return view('template.auth.plans',compact('client'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function adverts(){
        try {
            $client=Client::with('info','myCode','classrooms','races','adverts','plan','sendCV','wallet')->find(\auth('client')->id());
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('لیست آگهی ها');
            SEOMeta::setDescription('لیست آگهی ها');
            SEOMeta::setKeywords('لیست آگهی ها');
            SEOMeta::setCanonical(env('APP_URL').'/user/panel/adverts/cvs');
            return view('template.auth.advertisings',compact('client'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function advertCvs(){
        try {
            $client=Client::with('info','myCode','classrooms','races','adverts','plan','sendCV','wallet')->find(\auth('client')->id());
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('لیست درخواست  ها');
            SEOMeta::setDescription('لیست درخواست  ها');
            SEOMeta::setKeywords('لیست درخواست  ها');
            SEOMeta::setCanonical(env('APP_URL').'/user/panel/adverts/cvs');
            return view('template.auth.advert',compact('client'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }

    public function seekers(){
        try {
            $client=Client::with('info','myCode','classrooms','races','adverts','plan','sendCV','wallet')->find(\auth('client')->user()->id);
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('لیست کارجویان');
            SEOMeta::setDescription('لیست کارجویان');
            SEOMeta::setKeywords('لیست کارجویان');
            SEOMeta::setCanonical(env('APP_URL').'/user/panel/seekers');
            return view('template.auth.seekers',compact('client'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function wallet(){
        try {
            $client=Client::with('info','myCode','classrooms','races','adverts','plan','sendCV','wallet')->find(\auth('client')->user()->id);
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle(' کیف پول');
            SEOMeta::setDescription('کیف پول');
            SEOMeta::setKeywords('کیف پول');
            SEOMeta::setCanonical(env('APP_URL').'/user/panel/wallet');
            return view('template.auth.wallet',compact('client'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function edit(){
        try {
            $client=Client::with('info','myCode','classrooms','races','adverts','plan','sendCV','wallet')->find(\auth('client')->user()->id);
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('ویرایش پنل کاربری');
            SEOMeta::setDescription('ویرایش پنل کاربری');
            SEOMeta::setKeywords('ویرایش پنل کاربری');
            SEOMeta::setCanonical(env('APP_URL').'/user/panel/edit');
            return view('template.auth.edit',compact('client'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function registerReceiver(RegisterReceiverRequest $request){
        try {
            $client=Client::with('info','myCode','classrooms','races','adverts','plan','sendCV','wallet','cart')->find(\auth('client')->user()->id);

            $client->cart()->update([
                'mobile'=>$request->mobile,
                'address'=>$request->address,
                'first_name'=>$request->first_name,
                'last_name'=>$request->last_name,
                'postal_code'=>$request->postal_code,
            ]);
            return redirect(route('client.show.payment'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function selfInformation(){
        try {
            $client=Client::with('info','myCode','classrooms','races','adverts','plan','sendCV','wallet')->find(\auth('client')->user()->id);
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('اطلاعات شخصی');
            SEOMeta::setDescription('اطلاعات شخصی');
            SEOMeta::setKeywords('اطلاعات شخصی');
            SEOMeta::setCanonical(env('APP_URL').'/user/panel/self-information');
            return view('template.auth.edit',compact('client'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function update(ClientUpdateRequest $request,$client){
        try {
            $user=Client::findOrFail($client)->update([
                'mobile'=>$request->mobile,
                'email'=>$request->email,
                'first_name'=>$request->first_name,
                'address'=>$request->address,
                'postal_code'=>$request->postal_code,
                'identity_card'=>$request->identity_card,
                'last_name'=>$request->last_name,
            ]);

            if($request->has('username')){
                $user=Client::findOrFail($client)->update([
                    'username'=>$request->username,
                ]);
            }

            $item=Client::with('info')->findOrFail($client);

            if($request->has('image')){
                if($item->Hasmedia(config('cms.collection-image'))){
                    destroyMedia($item,config('cms.collection-image'));
                }
                $item->addMedia($request->file('image'))->toMediaCollection(config('cms.collection-image'));
            }

            if(!empty(trim($request->input('current_password'))) && !empty(trim($request->input('new_password')))  ){

                if(!$this->resetPasswordProfile($item,$request->input('current_password'),$request->input('new_password'))){
                    return redirect()->back()->with('error',__('user::users.error'));
                }

            }

            $item->info()->update([
                'website'=>$request->website,
                'youtube'=>$request->youtube,
                'github'=>$request->github,
                'facebook'=>$request->facebook,
                'twitter'=>$request->twitter,
                'telegram'=>$request->telegram,
                'instagram'=>$request->instagram,
                'whatsapp'=>$request->whatsapp,
                'pintrest'=>$request->pintrest,
                'about'=>$request->about,
                'address'=>$request->info_address,
                'country'=>$request->info_country,
            ]);

            if(!$user){
                return redirect()->back()->with('error',__('client::clients.error'));


            }else{
                return redirect()->back()->with('message',__('client::clients.update'));

            }

        }catch (\Exception $exception){
            return dd($exception->getMessage());
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function updateSeeker(SeekerUpdateRequest $request,$seeker){
        try {
            $user=Client::whereToken($seeker)->update([
                'email'=>$request->email,
                'username'=>$request->username,
                'identity_card'=>$request->identity_card,
                'first_name'=>$request->first_name,
                'last_name'=>$request->last_name,
            ]);

            $item=Client::with('info')->whereToken($seeker)->firstOrFail();

            if(!empty(trim($request->input('current_password'))) && !empty(trim($request->input('new_password')))  ){

                if(!$this->resetPasswordProfile($item,$request->input('current_password'),$request->input('new_password'))){
                    return redirect()->back()->with('error',__('user::users.error'));
                }
            }

            $item->info()->update([
                'website'=>$request->website,
                'youtube'=>$request->youtube,
                'github'=>$request->github,
                'facebook'=>$request->facebook,
                'twitter'=>$request->twitter,
                'telegram'=>$request->telegram,
                'instagram'=>$request->instagram,
                'whatsapp'=>$request->whatsapp,
                'pintrest'=>$request->pintrest,
                'about'=>$request->about,
                'address'=>$request->info_address,
                'country'=>$request->info_country,
            ]);

            if(!$user){
                return redirect()->back()->with('error',__('client::clients.error'));


            }else{
                return redirect()->back()->with('message',__('client::clients.update'));

            }


        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function femaleHair(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('female hair');
            SEOMeta::setDescription("");
            SEOMeta::setKeywords("");
            SEOMeta::setCanonical(env('APP_URL').'/hair/female');
            return view('template.forms.female_hair');

        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }

    public function sendEmail($data,$files){

        Mail::send('mail', $data, function($message) use($data,$files )
        {
            $message
                ->to('noorbaghaei.a2017@gmail.com')
                ->from('noorbaghaei.a2017@gmail.com')
                ->subject('amin nourbaghaei');
            foreach ($files as $key=>$value) {
                if(!empty(trim($value))){
                    $message->attach(public_path() . $value);
                }
            }

        });

    }
    public function specialProducts(){
        try {
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('محصولات شگفت انگیز');
            $items=Product::with('attributes','properties','price')->whereSpecial(2)->get();
            SEOMeta::setCanonical(env('APP_URL').'/products');
            return view('template.products.special',compact('items'));

        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function searchCompany(Request $request,$category){
        try {

            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('جستحو');
            $items= Client::with('company','info')->latest()
                ->whereHas('role',function ($query){
                    $query->where('title', '=', 'employer');
                })->where('category',$category)->get();
            SEOMeta::setCanonical(env('APP_URL').'/products');
            return view('template.companies.search',compact('items'));

        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }

    public function shopFilter(Request $request){

        try {
            $array_category=[];
            $order="";
            $query=Product::query();
            $query=$query->with('info_company')->whereStatus(1);
            if($request->order){

                if($request->order=='new')
                {
                    $order=$request->order;
                    $query=$query->latest();
                }
                elseif(($request->order=='view')){
                    $order=$request->order;
                    $query=$query->whereHas('analyzer', function ($q) {
                        return $q->orderBy('view','desc');
                    });
                }

            }
            if($request->category){
                $query=$query->whereIn('category',$request->category);
                $array_category=$request->category;
            }
            $query=$query->paginate(config('cms.paginate'));
            $categories=Category::whereModel(Product::class)->whereIn('status',[1,3])->whereParent(0)->get();
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('فروشگاه');
            SEOMeta::setCanonical(env('APP_URL').'/shop');
            return view('template.shop.index',compact('query','categories','array_category','order'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }


    }
    public function advertFilter(Request $request){

        try {
            $array_category=[];
            $array_company=[];
            $array_skill=[];
            $item_salary="";
            $order="";
            $query=Advertising::query();
            $query=$query->with('info_company')->whereStatus(2);
            if($request->order){

                if($request->order=='new')
                {
                    $order=$request->order;
                    $query=$query->latest();
                }
                elseif(($request->order=='view')){
                    $order=$request->order;
                   $query=$query->whereHas('analyzer', function ($q) {
                       return $q->orderBy('view','desc');
                   });
                }

            }
            if($request->category){
                $query=$query->whereIn('category',$request->category);
                $array_category=$request->category;
            }
            if($request->company){
                $company=$request->company;
                $query=$query->whereHas('info_company', function ($q) use ($company) {
                    return $q->whereIn('id', $company);
                });
                $array_company=$request->company;
            }
            if($request->skill){
                $array_skill=$request->skill;
                $query=$query->whereJsonContains('skill',$array_skill);
            }
            if($request->salary){
                $item_salary=$request->salary;
                $query=$query->where('salary',$item_salary);
            }

             $query=$query->paginate(config('cms.paginate'));
            $categories=Category::whereModel(Product::class)->whereIn('status',[1,3])->whereParent(0)->get();
            SEOMeta::setTitleDefault(\setting('name'));
            SEOMeta::setTitle('جستجو آگهی ها');
            SEOMeta::setCanonical(env('APP_URL').'/advertising');
            return view('template.advertisings.index',compact('query','categories','array_category','array_company','item_salary','array_skill','order'));
        }catch (\Exception $exception){

            Rest::errorLog($exception->getMessage());
            return abort('500');
        }


    }

    public function RegisterClassroom(Request $request,$classroom){
        try {
            $classroom=ClassRoom::whereToken($classroom)->with('price')->firstOrFail();
            if($classroom->price->amount!=0 || !auth('client')->check() || hasAction($classroom,auth('client')->user(),'classroom')){
                return abort('500');
            }
            if(hasChild($classroom,'classroom')){
                UserAction::create([
                    'actionable_id'=>$classroom->id,
                    'actionable_type'=>ClassRoom::class,
                    'client'=>auth('client')->user()->id,
                    'status'=>1,
                    'title'=>"ثبت نام",
                    'text'=>"ثبت نام رایگان",
                ]);

                foreach (getChild($classroom,'classroom') as $child){
                    UserAction::create([
                        'actionable_id'=>$child->id,
                        'actionable_type'=>ClassRoom::class,
                        'client'=>auth('client')->user()->id,
                        'status'=>1,
                        'title'=>"ثبت نام",
                        'text'=>"ثبت نام رایگان",
                    ]);
                }


            }else{
                UserAction::create([
                    'actionable_id'=>$classroom->id,
                    'actionable_type'=>ClassRoom::class,
                    'client'=>auth('client')->user()->id,
                    'status'=>1,
                    'title'=>"ثبت نام",
                    'text'=>"ثبت نام رایگان",
                ]);
            }

            return redirect()->back()->with('message',__('educational::classrooms.store'));



        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }

    }
    public  function RegisterRace(Request $request,$race){
        try {
            $race=Race::whereToken($race)->with('price')->whereParent(0)->firstOrFail();
            if($race->price->amount!=0 || !auth('client')->check() || hasAction($race,auth('client')->user(),'race')){
                return abort('500');
            }
            if(hasChild($race,'race')){
                UserAction::create([
                    'actionable_id'=>$race->id,
                    'actionable_type'=>Race::class,
                    'client'=>auth('client')->user()->id,
                    'status'=>1,
                    'title'=>"ثبت نام",
                    'text'=>"ثبت نام رایگان",
                ]);

                foreach (getChild($race,'race') as $child){
                    UserAction::create([
                        'actionable_id'=>$child->id,
                        'actionable_type'=>Race::class,
                        'client'=>auth('client')->user()->id,
                        'status'=>1,
                        'title'=>"ثبت نام",
                        'text'=>"ثبت نام رایگان",
                    ]);
                }


            }else{
                UserAction::create([
                    'actionable_id'=>$race->id,
                    'actionable_type'=>ClassRoom::class,
                    'client'=>auth('client')->user()->id,
                    'status'=>1,
                    'title'=>"ثبت نام",
                    'text'=>"ثبت نام رایگان",
                ]);
            }

            return redirect()->back()->with('message',__('educational::races.store'));

        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function sendCv(CvSendRequest $request,$token){
        try {
            $data=Advertising::whereToken($token)->whereStatus(2)->firstOrFail();

            $action=UserAction::create([
                'actionable_id'=>$data->id,
                'actionable_type'=>Advertising::class,
                'client'=>auth('client')->user()->id,
                'status'=>1,
                'title'=>"درخواست کار",
                'text'=>"ارسال رزومه برای کارفرما",
            ]);

            if($request->file('document')){
                $action->addMedia($request->file('document'))->toMediaCollection('cv');
            }

            if(!$action){
                return redirect()->back()->with('error',__('advertising::advertisings.error'));

            }else{
                return redirect()->back()->with('message',__('advertising::advertisings.store_cv'));
            }


        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function sendIdea(Request $request){
        try {


            $data=[
                'name'=>$request->name,
                'email'=>$request->email,
                'subject'=>$request->subject,
                'message'=>$request->message,
            ];

            sendCustomEmail($data,'emails.front.contact');
            return redirect()->back();



        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }

    public function allCountry(){

            return Country::OrderBy('name','desc')->get();
    }
    public function getStates($country){


            return State::where('country',$country)->OrderBy('name','desc')->get();
    }
    public function addToCart(Request $request,$product){
        try {

            $product=Product::with('price')->whereToken($product)->firstOrFail();

            $client=Client::findOrFail(\auth('client')->user()->id);



            $cart=$client->cart()->updateOrCreate([
                'client'=>$client->id,
                'mobile'=>$client->mobile
            ]);

            $cart=Cart::findOrFail($cart->id);

            if(CartList::where('product',$product->id)->where('cart',$cart->id)->first()){
                $cartlist=CartList::where('product',$product->id)->where('cart',$cart->id)->first();
                $cartlist->update([
                    'count'=>$cartlist->count + 1,
                    'price'=>$cartlist->price + intval($product->price->amount)
                ]);
            }else{
                CartList::create([
                    'cart'=>$cart->id,
                    'product'=>$product->id,
                    'price'=>intval($product->price->amount)
                ]);

            }

            return redirect(route('client.show.cart'));
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }

    public function getCities($state){



        return City::where('state',$state)->OrderBy('name','desc')->get();
    }

    public function sendTransactionRequest($name,$item=null){

        try {
            $user=Client::with('cart')->find(auth('client')->user()->id);
            switch ($name){
                case 'classRoom':
                    $result=ClassRoom::with('price')->whereToken($item)->firstOrFail();
                    break;

                case 'race':
                    $result=Race::with('price')->whereToken($item)->firstOrFail();
                    break;
                case 'product':
                   $result=$user->cart;
                    break;
                default:
                    break;
            }
           if(count($user->cart->items) > 0){
               $data=[
                   'amount'=>$user->cart->items->sum('price'),
                   'title'=>'پرداخت الکترونیکی',
                   'description'=>'پرداخت مرسوله',
                   'email'=>'noorbaghaei.a2017@gmail.com',
                   'mobile'=>'09195995044 ',
               ];
           }else{
               $data=[
                   'amount'=>$result->price->amount,
                   'title'=>'پرداخت الکترونیکی',
                   'description'=>'پرداخت شهریه',
                   'email'=>'noorbaghaei.a2017@gmail.com',
                   'mobile'=>'09195995044 ',
               ];
           }

               ZarinPal::request($data,$result);
        }catch (\Exception $exception){
            Rest::errorLog($exception->getMessage());
            return abort('500');
        }
    }
    public function callbackTransactionMessage($error,$payment){
        $message="";

        if($error==1){
            $message="عملیات پرداخت ناموفق بود";

        }elseif($error==2){
            $message="عملیات پرداخت با موفقیت انجام شد";
        }
        elseif($error==0){
            $message="شما از عملیات درگاه بانک انصراف دادید";
        }
        return view('template.transactions.message',compact('message','error','payment'));
    }
    public function callbackTransaction(){

        $zarinpal=new ZarinPal();
        return $zarinpal->response();
    }




}
