<div class="container">
    <h2 class="section_header fancy centered">نمونه کار ها</h2>
    <div class="portfolio_strict row">
        @foreach($portfolios as $portfolio)
        <div class="col-sm-4 col-md-4">
            <div class="portfolio_item wow fadeInUp"> <a href="#">
                    <figure style="background-image:url({{asset('template/images/portfolio/b3.jpg')}})">
                        <figcaption>
                            <div class="portfolio_description">
                                <h3>{{$portfolio->title}}</h3>
                                <span class="cross"></span>
                                <p>{{$portfolio->excerpt}}</p>
                            </div>
                        </figcaption>
                    </figure>
                </a> </div>
        </div>
        @endforeach

    </div>
    <div class="centered_button"><a class="btn btn-primary" href="{{route('portfolios')}}">نمونه های دیگر</a></div>
</div>
