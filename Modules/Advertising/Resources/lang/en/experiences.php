<?php
return [
    "text-create"=>"you can create your experience",
    "text-edit"=>"you can edit your skill",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"experiences list",
    "error"=>"error",
    "singular"=>"experience",
    "collect"=>"experiences",
    "permission"=>[
        "experience-full-access"=>"experience full access",
        "experience-list"=>"experiences list",
        "experience-delete"=>"experience delete",
        "experience-create"=>"experience create",
        "experience-edit"=>"experience skill",
    ]


];
