@include('core::layout.modules.index',[

    'title'=>__('core::roles.index'),
    'items'=>$items,
    'model'=>'role',
    'collect'=>__('core::roles.collect'),
    'singular'=>__('core::roles.singular'),
    'create_route'=>['name'=>'roles.create'],
    'edit_route'=>['name'=>'roles.edit','name_param'=>'role'],
    'datatable'=>[
    __('cms.title')=>'name',
    __('cms.update_date')=>'AgoTimeUpdate',
    __('cms.create_date')=>'TimeCreate',
    ],
        'detail_data'=>[
    __('cms.title')=>'name',
    __('cms.create_date')=>'created_at',
    __('cms.update_date')=>'updated_at',
    ],


])
