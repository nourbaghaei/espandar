﻿@extends('template.app')

@section('content')

@include('template.sections.sliders')

    <!--body content start-->

    <div class="page-content">

        <!--about us start-->

        <section>
            <div class="container">
                <div class="row text-center">
                    <div class="col-lg-8 col-md-10 ml-auto mr-auto">
                        <div class="section-title">
                            <h2 class="title">امکانات</h2>
                        </div>
                    </div>
                </div>
                <div class="row align-items-center">
                    <div class="col-lg-6 col-md-12">
                        <div class="row">
                            <div class="col-md-6 pr-2">
                                <div class="about-img mb-3">
                                    <img class="img-fluid" src="{{asset('template/images/about/02.jpg')}}" alt="">
                                </div>
                                <div class="about-img">
                                    <img class="img-fluid" src="{{asset('template/images/about/01.jpg')}}" alt="">
                                </div>
                            </div>
                            <div class="col-md-6 mt-4 pl-2">
                                <div class="about-img mb-3">
                                    <img class="img-fluid" src="{{asset('template/images/about/03.jpg')}}" alt="">
                                </div>
                                <div class="about-img">
                                    <img class="img-fluid" src="{{asset('template/images/about/04.jpg')}}" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 box-shadow white-bg px-4 py-4 sm-px-3 sm-py-3 xs-py-2 xs-px-2 md-mt-5">
                        <h5>امکانات </h5>
                        <p class="line-h-3">متن امکانات</p>
                        <div class="row text-black mt-4">
                            <div class="col-sm-6">
                                <ul class="list-unstyled">
                                    <li class="mb-2">- مهندسین خبره</li>
                                    <li class="mb-2">- طراحی در کیفیت</li>
                                    <li>- بهترین نام تجاری</li>
                                </ul>
                            </div>
                            <div class="col-sm-6 xs-mt-3">
                                <ul class="list-unstyled">
                                    <li class="mb-2">- مهندسین کارشناسی</li>
                                    <li class="mb-2">- تمامیت</li>
                                    <li>- مهندسین خبره</li>
                                </ul>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-md-12">
                                <div class="ht-progress-bar style-2">
                                    <h4>قرارداد</h4>
                                    <div class="progress" data-value="90">
                                        <div class="progress-bar">
                                            <div class="progress-parcent"><span>90</span>%</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="ht-progress-bar style-2">
                                    <h4>لوله کشی</h4>
                                    <div class="progress" data-value="65">
                                        <div class="progress-bar">
                                            <div class="progress-parcent"><span>65</span>%</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--about us end-->


@include('template.sections.services')

        <!--feauture start-->

        <section class="grey-bg pattern feuture-bottom white-overlay" data-bg-img="{{asset('template/images/pattern/01.png')}}" data-overlay="3">
            <div class="container">
                <div class="row no-gutters align-items-center">
                    <div class="col-lg-6 col-md-6 pr-lg-5 md-px-2 text-center">
                        <div class="section-title mb-md-0">
                            <h2 class="title">امکانات   </h2>
                            <p class="mb-0">متن</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="featured-item bottom-icon">
                            <div class="featured-title text-uppercase">
                                <h5>آخرین تکنولوژی</h5>
                            </div>
                            <div class="featured-desc">
                                <p> متن</p>
                            </div>
                            <div class="featured-icon"> <i class="flaticon-innovation"></i>
                            </div> <span><i class="flaticon-innovation"></i></span>
                        </div>
                    </div>
                </div>
                <div class="row no-gutters">
                    <div class="col-lg-3 col-md-6">
                        <div class="featured-item bottom-icon">
                            <div class="featured-title text-uppercase">
                                <h5>همیشه متصل است</h5>
                            </div>
                            <div class="featured-desc">
                                <p>متن</p>
                            </div>
                            <div class="featured-icon"> <i class="flaticon-chat-bubble"></i>
                            </div> <span><i class="flaticon-chat-bubble"></i></span>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="featured-item bottom-icon">
                            <div class="featured-title text-uppercase">
                                <h5>تیم کارشناس</h5>
                            </div>
                            <div class="featured-desc">
                                <p>متن</p>
                            </div>
                            <div class="featured-icon"> <i class="flaticon-employee"></i>
                            </div> <span><i class="flaticon-employee"></i></span>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="featured-item bottom-icon">
                            <div class="featured-title text-uppercase">
                                <h5>آسان و سریع</h5>
                            </div>
                            <div class="featured-desc">
                                <p>متن</p>
                            </div>
                            <div class="featured-icon"> <i class="flaticon-innovation-1"></i>
                            </div> <span><i class="flaticon-innovation-1"></i></span>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="featured-item bottom-icon">
                            <div class="featured-title text-uppercase">
                                <h5>زمان تحویل</h5>
                            </div>
                            <div class="featured-desc">
                                <p>متن </p>
                            </div>
                            <div class="featured-icon"> <i class="flaticon-alarm-clock"></i>
                            </div> <span><i class="flaticon-alarm-clock"></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--feauture end-->


        <!--project start-->

        <section class="o-hidden">
            <div class="container">
                <div class="row text-center">
                    <div class="col-lg-8 col-md-10 ml-auto mr-auto">
                        <div class="section-title">
                            <h2 class="title">نمونه کارها</h2>
                        </div>
                    </div>
                </div>
                <div class="row text-center">
                    <div class="col-md-12">
                        <div class="portfolio-filter">
                            <button data-filter="" class="is-checked">همه</button>
                            <button data-filter=".cat1">مکانیکی</button>
                            <button data-filter=".cat2">لوله کشی</button>
                            <button data-filter=".cat3">جوشکاری</button>
                            <button data-filter=".cat4">شیمیایی</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid p-0">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="masonry row columns-4 no-gutters popup-gallery">
                            <div class="grid-sizer"></div>
                            <div class="masonry-brick cat3">
                                <div class="portfolio-item">
                                    <img src="{{asset('template/images/portfolio/masonry/01.jpg')}}" alt="">
                                    <div class="portfolio-hover">
                                        <div class="portfolio-title"> <span>جوش، شیمیایی</span>
                                            <h4>عنوان پروژه</h4>
                                        </div>
                                        <div class="portfolio-icon">
                                            <a class="popup popup-img" href="{{asset('template/images/portfolio/large/01.jpg')}}"> <i class="flaticon-magnifier"></i>
                                            </a>
                                            <a class="popup portfolio-link" target="_blank" href="#"> <i class="flaticon-broken-link-1"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="masonry-brick cat3">
                                <div class="portfolio-item">
                                    <img src="{{asset('template/images/portfolio/masonry/03.jpg')}}" alt="">
                                    <div class="portfolio-hover">
                                        <div class="portfolio-title"> <span>جوش، شیمیایی</span>
                                            <h4>عنوان پروژه</h4>
                                        </div>
                                        <div class="portfolio-icon">
                                            <a class="popup popup-img" href="{{asset('template/images/portfolio/large/03.jpg')}}"> <i class="flaticon-magnifier"></i>
                                            </a>
                                            <a class="popup portfolio-link" target="_blank" href="#"> <i class="flaticon-broken-link-1"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="masonry-brick cat4">
                                <div class="portfolio-item">
                                    <img src="images/portfolio/masonry/02.jpg" alt="">
                                    <div class="portfolio-hover">
                                        <div class="portfolio-title"> <span>جوش، شیمیایی</span>
                                            <h4>عنوان پروژه</h4>
                                        </div>
                                        <div class="portfolio-icon">
                                            <a class="popup popup-img" href="{{asset('template/images/portfolio/large/02.jpg')}}"> <i class="flaticon-magnifier"></i>
                                            </a>
                                            <a class="popup portfolio-link" target="_blank" href="#"> <i class="flaticon-broken-link-1"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="masonry-brick cat2">
                                <div class="portfolio-item">
                                    <img src="{{asset('template/images/portfolio/masonry/04.jpg')}}" alt="">
                                    <div class="portfolio-hover">
                                        <div class="portfolio-title"> <span>جوش، شیمیایی</span>
                                            <h4>عنوان پروژه</h4>
                                        </div>
                                        <div class="portfolio-icon">
                                            <a class="popup popup-img" href="{{asset('template/images/portfolio/large/04.jpg')}}"> <i class="flaticon-magnifier"></i>
                                            </a>
                                            <a class="popup portfolio-link" target="_blank" href="#"> <i class="flaticon-broken-link-1"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="masonry-brick cat1">
                                <div class="portfolio-item">
                                    <img src="{{asset('template/images/portfolio/masonry/07.jpg')}}" alt="">
                                    <div class="portfolio-hover">
                                        <div class="portfolio-title"> <span>جوش، شیمیایی</span>
                                            <h4>عنوان پروژه</h4>
                                        </div>
                                        <div class="portfolio-icon">
                                            <a class="popup popup-img" href="{{asset('template/images/portfolio/large/07.jpg')}}"> <i class="flaticon-magnifier"></i>
                                            </a>
                                            <a class="popup portfolio-link" target="_blank" href="#"> <i class="flaticon-broken-link-1"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="masonry-brick cat1">
                                <div class="portfolio-item">
                                    <img src="{{asset('template/images/portfolio/masonry/08.jpg')}}" alt="">
                                    <div class="portfolio-hover">
                                        <div class="portfolio-title"> <span>جوش، شیمیایی</span>
                                            <h4>عنوان پروژه</h4>
                                        </div>
                                        <div class="portfolio-icon">
                                            <a class="popup popup-img" href="{{asset('template/images/portfolio/large/08.jpg')}}"> <i class="flaticon-magnifier"></i>
                                            </a>
                                            <a class="popup portfolio-link" target="_blank" href="#"> <i class="flaticon-broken-link-1"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="masonry-brick cat3">
                                <div class="portfolio-item">
                                    <img src="{{asset('template/images/portfolio/masonry/06.jpg')}}" alt="">
                                    <div class="portfolio-hover">
                                        <div class="portfolio-title"> <span>جوش، شیمیایی</span>
                                            <h4>عنوان پروژه</h4>
                                        </div>
                                        <div class="portfolio-icon">
                                            <a class="popup popup-img" href="{{asset('template/images/portfolio/large/06.jpg')}}"> <i class="flaticon-magnifier"></i>
                                            </a>
                                            <a class="popup portfolio-link" target="_blank" href="#"> <i class="flaticon-broken-link-1"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="masonry-brick cat4">
                                <div class="portfolio-item">
                                    <img src="{{asset('template/images/portfolio/masonry/05.jpg')}}" alt="">
                                    <div class="portfolio-hover">
                                        <div class="portfolio-title"> <span>جوش، شیمیایی</span>
                                            <h4>عنوان پروژه</h4>
                                        </div>
                                        <div class="portfolio-icon">
                                            <a class="popup popup-img" href="{{asset('template/images/portfolio/large/05.jpg')}}"> <i class="flaticon-magnifier"></i>
                                            </a>
                                            <a class="popup portfolio-link" target="_blank" href="#"> <i class="flaticon-broken-link-1"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="masonry-brick cat4">
                                <div class="portfolio-item">
                                    <img src="{{asset('template/images/portfolio/masonry/09.jpg')}}" alt="">
                                    <div class="portfolio-hover">
                                        <div class="portfolio-title"> <span>جوش، شیمیایی</span>
                                            <h4>عنوان پروژه</h4>
                                        </div>
                                        <div class="portfolio-icon">
                                            <a class="popup popup-img" href="{{asset('template/images/portfolio/large/09.jpg')}}"> <i class="flaticon-magnifier"></i>
                                            </a>
                                            <a class="popup portfolio-link" target="_blank" href="#"> <i class="flaticon-broken-link-1"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="masonry-brick cat1">
                                <div class="portfolio-item">
                                    <img src="{{asset('template/images/portfolio/masonry/10.jpg')}}" alt="">
                                    <div class="portfolio-hover">
                                        <div class="portfolio-title"> <span>جوش، شیمیایی</span>
                                            <h4>عنوان پروژه</h4>
                                        </div>
                                        <div class="portfolio-icon">
                                            <a class="popup popup-img" href="{{asset('template/images/portfolio/large/10.jpg')}}"> <i class="flaticon-magnifier"></i>
                                            </a>
                                            <a class="popup portfolio-link" target="_blank" href="#"> <i class="flaticon-broken-link-1"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--project end-->




        <!--multi section start-->

        <section>
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6 col-md-12">
                        <div class="owl-carousel owl-theme no-pb xs-text-center" data-dots="false" data-items="3" data-margin="30" data-autoplay="true">
                            <div class="item">
                                <img class="img-center" src="{{asset('template/images/client/03.png')}}" alt="">
                            </div>
                            <div class="item">
                                <img class="img-center" src="{{asset('template/images/client/04.png')}}" alt="">
                            </div>
                            <div class="item">
                                <img class="img-center" src="{{asset('template/images/client/03.png')}}" alt="">
                            </div>
                            <div class="item">
                                <img class="img-center" src="{{asset('template/images/client/04.png')}}" alt="">
                            </div>
                        </div>
                        <div class="row mt-5">
                            <div class="col-sm-12">
                                <div class="tab">
                                    <!-- Nav tabs -->
                                    <nav>
                                        <div class="nav nav-tabs" id="nav-tab" role="tablist"> <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#tab1-1" role="tab" aria-selected="true">ماموریت ما</a>
                                            <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#tab1-2" role="tab" aria-selected="false">دیدگاه ما</a>
                                        </div>
                                    </nav>
                                    <!-- Tab panes -->
                                    <div class="tab-content pl-3 pt-3 pb-0" id="nav-tabContent">
                                        <div role="tabpanel" class="tab-pane fade show active" id="tab1-1">
                                            <p class="mb-0 lead">متن</p>
                                            <div class="row text-black mt-4">
                                                <div class="col-sm-6">
                                                    <ul class="list-unstyled">
                                                        <li class="mb-2">- مهندسین خبره</li>
                                                        <li class="mb-2">- طراحی در کیفیت</li>
                                                        <li>- بهترین نام تجاری</li>
                                                    </ul>
                                                </div>
                                                <div class="col-sm-6 xs-mt-3">
                                                    <ul class="list-unstyled">
                                                        <li class="mb-2">- مهندسین کارشناسی</li>
                                                        <li class="mb-2">- تمامیت</li>
                                                        <li>- مهندسین خبره</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="tab1-2">
                                            <p class="mb-0 lead">متن</p>
                                            <div class="row text-black mt-4">
                                                <div class="col-sm-6">
                                                    <h6 class="mb-2">- مهندسین خبره</h6>
                                                    <h6 class="mb-2">- طراحی در کیفیت</h6>
                                                    <h6>- بهترین نام تجاری</h6>
                                                </div>
                                                <div class="col-sm-6 xs-mt-3">
                                                    <h6 class="mb-2">- مهندسین کارشناسی</h6>
                                                    <h6 class="mb-2">- تمامیت</h6>
                                                    <h6>- مهندسین خبره</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 md-mt-5">
                        <div id="accordion" class="accordion style-1">
                            <div class="card active">
                                <div class="card-header">
                                    <h6 class="mb-0">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true">سوال ؟</a>
                                    </h6>
                                </div>
                                <div id="collapse1" class="collapse show" data-parent="#accordion">
                                    <div class="card-body">جواب </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header">
                                    <h6 class="mb-0">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true">سوال ؟</a>
                                    </h6>
                                </div>
                                <div id="collapse1" class="collapse" data-parent="#accordion">
                                    <div class="card-body">جواب </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header">
                                    <h6 class="mb-0">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true">سوال ؟</a>
                                    </h6>
                                </div>
                                <div id="collapse1" class="collapse" data-parent="#accordion">
                                    <div class="card-body">جواب </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--multi section end-->




  @include('template.sections.articles')


    </div>

    <!--body content end-->

@include('template.sections.brands')

@endsection



