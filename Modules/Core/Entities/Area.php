<?php

namespace Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $fillable = ['city_id'];

    protected $table ="areas";

}
