@if(\Modules\Core\Helper\CoreHelper::hasEvent())
<section >
    <div class="container-fluid plp-container">
        <div class="plp-box">
            <strong class="section-title-inline-1">
                <h1> News & Events </h1>
            </strong>
        </div>
    </div>

    <div class="news-slider-content">
        <div class="container-fluid ">
            <div class="row d-flex align-items-center">
                <div class="col-lg-7 col-md-7 ">
                    <div class="swiper-container news-left-slider">
                        <!-- Swiper -->
                        <div class="swiper-wrapper">
                            @foreach($events as $event)
                                <div class="swiper-slide">
                                <div class="box-container">
                                    <div class="left-main-box">
                                        <div class="row text-box">
                                            <div class="col-lg-4 col-md-4 col-sm-4">
                                                <div class="img-box">
                                                    <img src="{{$event->getFirstMediaUrl('images','thumb')}}" alt="" width="220px" height="115px">
                                                </div>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8">
                                                <a href="{{route('events')}}">
                                                    <div class="right-text">
                                                        {!! $event->text !!}
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-md-5  right-slider-content">
                    <!-- Swiper -->
                    <div class="swiper-container news-right-slider">
                        <div class="swiper-wrapper">
                            @foreach($events as $key=>$event)
                                @if($key % 2 ==0)
                            <div class="swiper-slide">
                                <div class="box-container">
                                    <div class="main-box">
                                        <div class="text-box">
                                            <div class="left-text">
                                                <a href="#"><h2> {{$event->title}} </h2></a>
                                                <span> {{$event->country}} </span>
                                            </div>
                                            <div class="right-text">
                                                <span> {{$event->MonthYear }} </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                @else
                                    <div class="swiper-slide"><div class="box-container">
                                            <div class="top-border"></div>
                                            <div class="main-box second">
                                                <div class="text-box">
                                                    <div class="left-text">
                                                        <a href="#"><h2> {{$event->title}} </h2></a>
                                                        <span> {{$event->country}} </span>
                                                    </div>
                                                    <div class="right-text">
                                                        <span> {{$event->MonthYear}} </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div></div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endif
