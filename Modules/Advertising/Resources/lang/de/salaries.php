<?php
return [
    "text-create"=>"you can create your salary",
    "text-edit"=>"you can edit your salary",
    "store"=>"Store Success",
    "delete"=>"Delete Success",
    "update"=>"Update Success",
    "index"=>"salaries list",
    "error"=>"error",
    "singular"=>"salary",
    "collect"=>"salaries",
    "permission"=>[
        "salary-full-access"=>"salary full access",
        "salary-list"=>"salaries list",
        "salary-delete"=>"salary delete",
        "salary-create"=>"salary create",
        "salary-edit"=>"edit salary",
    ]


];
