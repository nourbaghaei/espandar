<div class="app-footer white bg p-a b-t">
    <div class="pull-right text-sm text-muted">{{__('cms.version')}} {{config('cms.version')}}</div>
    <span class="text-sm text-muted">{{__('cms.copy')}}</span>
</div>
