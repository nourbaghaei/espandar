<!--blog start-->

<section class="pb-17 sm-pb-8">
    <div class="container">
        <div class="row text-center">
            <div class="col-lg-8 col-md-10 ml-auto mr-auto">
                <div class="section-title">
                    <h2 class="title">مقالات</h2>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach($new_articles as $article)
            <div class="col-lg-4 col-md-12">
                <div class="post">
                    <div class="post-image">
                                            @if(!$article->Hasmedia('images'))
                            <img src="{{asset('img/no-img.gif')}}" alt="{{$article->title}}" title="{{$article->title}}">

                                                    @else
                            <img src="{{$article->getFirstMediaUrl('images')}}" alt="{{$article->title}}" title="{{$article->title}}">

                                                    @endif

                        <div class="post-date">{{Morilog\Jalali\Jalalian::forge($article->create_at)->format('%A, %d %B %y')}}</div>
                    </div>
                    <div class="post-desc">
                        <div class="post-title">
                            <h5><a href="#">{{$article->title}}</a></h5>
                        </div>
                            <p>
                                {{$article->excerpt}}
                            </p>
                    </div>
                    <div class="post-bottom">
                        <div class="post-meta">
                            <ul class="list-inline">
                                <li>بازدید</li>
                            </ul>
                        </div> <a class="post-btn" href="#">ادامه مطلب <i class="mr-2 fas fa-long-arrow-alt-left"></i></a>
                    </div>
                </div>
            </div>
            @endforeach


        </div>
    </div>
</section>

