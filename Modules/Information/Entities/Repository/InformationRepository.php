<?php


namespace Modules\Information\Entities\Repository;


use Modules\Information\Entities\Information;

class InformationRepository implements InformationRepositoryInterface
{

    public function getAll()
    {
        return Information::latest()->get();
    }
}
