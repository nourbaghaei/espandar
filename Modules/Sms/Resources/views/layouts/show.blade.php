@extends('core::layout.panel')
@section('pageTitle', 'پیام رسان')
@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <div class="pull-left">
                            <h2>پیام رسان  شرکت {{$smse->provider}}</h2>
                            <small>توضیحات پیام رسان</small>
                        </div>
                        <a onclick="window.print()" class="btn btn-primary btn-sm text-sm text-white pull-right">چاپ پیام رسان</a>
                    </div>
                    <br>
                    <br>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
                        <div class="form-group row">
                            <div class="col-sm-4">
                                <label for="title" class="form-control-label">شرکت :  </label>
                                <span>{{$smse->provider}}</span>
                            </div>

                        </div>
                        <div class="form-group row">

                            <div class="col-sm-12">
                                <label for="text" class="form-control-label">توکن :  </label>
                                <br>
                                <span>{!! $smse->key !!}</span>
                            </div>

                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
