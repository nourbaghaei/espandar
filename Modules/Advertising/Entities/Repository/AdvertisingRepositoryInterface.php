<?php


namespace Modules\Advertising\Entities\Repository;


interface AdvertisingRepositoryInterface
{
    public function getAll();
}
