<?php


namespace Modules\Brand\Entities\Repository;


interface BrandRepositoryInterface
{

    public function getAll();
}
