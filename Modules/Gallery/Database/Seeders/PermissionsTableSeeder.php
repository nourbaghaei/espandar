<?php

namespace Modules\Gallery\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Core\Entities\Contact;
use Modules\Gallery\Entities\Gallery;
use Spatie\Permission\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('permissions')->whereModel(Gallery::class)->delete();

        Permission::create(['name'=>'gallery-list','model'=>Gallery::class,'created_at'=>now()]);
        Permission::create(['name'=>'gallery-create','model'=>Gallery::class,'created_at'=>now()]);
        Permission::create(['name'=>'gallery-edit','model'=>Gallery::class,'created_at'=>now()]);
        Permission::create(['name'=>'gallery-delete','model'=>Gallery::class,'created_at'=>now()]);

    }
}
